#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct ErrorResponse
{
	std::string message;
};

inline void to_json(json& j, const ErrorResponse& p)
{
	j = json{ { "message", p.message } };
}

inline void from_json(const json& j, ErrorResponse& p)
{
	p.message = j.at("message").get<std::string>();
}