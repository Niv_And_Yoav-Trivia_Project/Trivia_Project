#include "Room.h"

Room::Room()
{
}

Room::Room(RoomData RD, LoggedUser user) : m_metadata(RD)
{
	m_users.push_back(user);
}

void Room::addUser(LoggedUser user)
{
	if (m_users.size() < m_metadata.maxPlayers)
		m_users.push_back(user);
}

void Room::removeUser(LoggedUser user)
{
	int place = 0;

	for (int i = 0; i < m_users.size(); i++)
	{
		if (user.getUsername() == m_users[i].getUsername())
		{
			place = i;
		}
	}
	m_users.erase(m_users.begin() + place);
}

vector<LoggedUser>& Room::getAllUsers()
{
	return m_users;
}

RoomData Room::getRoomData() const
{
	return m_metadata;
}

void Room::SetIsActive(bool isActive)
{
	m_metadata.isActive = isActive;
}