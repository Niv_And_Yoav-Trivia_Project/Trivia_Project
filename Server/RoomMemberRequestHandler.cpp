#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(Room room, LoggedUser user, RoomManager& manager, RequestHandlerFactory& factory) : m_room(room), m_user(user), m_roomManager(manager), m_handlerFactory(factory)
{
	getRoomStateFlag = true;
	roomClosed = false;
	gameStarted = false;
}

bool RoomMemberRequestHandler::isRequestRelevant(Request request)
{
	int code = (int)request.buffer[0];
	if (code >= GET_ROOM_STATE_CODE && code <= LEAVE_ROOM_CODE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

RequestResult RoomMemberRequestHandler::handleRequest(Request request, SOCKET clientSocket)
{
	switch (request.id)
	{
	case GET_ROOM_STATE_CODE:
		return getRoomState(request, clientSocket);
		break;
	case LEAVE_ROOM_CODE:
		return leaveRoom(request);
		break;
	default:
		ErrorResponse response{ "Code doesnt exist" };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
		break;
	}
}

RequestResult RoomMemberRequestHandler::leaveRoom(Request request)
{
	std::map<unsigned int, Room>::iterator it;
	std::map<unsigned int, Room> map(m_roomManager.getRoomMap());
	int roomId = 0;
	bool found = false;
	for (it = map.begin(); it != map.end(); ++it)
	{
		std::vector<LoggedUser> v(it->second.getAllUsers());
		for (std::vector<LoggedUser>::iterator it2 = v.begin(); it2 != v.end(); ++it2)
		{
			if (it2->getUsername() == m_user.getUsername())
			{
				roomId = it->first;
				found = true;
			}
		}
	}
	if (found)
	{
		m_roomManager.getRoomMap()[roomId].removeUser(m_user.getUsername());
		LeaveRoomResponse response{ SUCCEEDED };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		IRequestHandler* handler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(m_user));
		RequestResult result{ buffer, handler };
		return result;
	}
	else
	{
		LeaveRoomResponse response{ UNABLE_TO_LEAVE_ROOM };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
}

void RoomMemberRequestHandler::getRoomStateThread(Request request, SOCKET clientSocket)
{
	std::vector<string> Loggedusers;
	while (!getRoomStateFlag)
	{
		std::map<unsigned int, Room>::iterator it;
		vector<string> users;
		std::map<unsigned int, Room> map(m_roomManager.getRoomMap());
		for (it = map.begin(); it != map.end(); ++it)
		{
			if (it->first == m_room.getRoomData().id)
			{
				std::vector<LoggedUser> currentUsers(it->second.getAllUsers());
				for (std::vector<LoggedUser>::iterator it2 = currentUsers.begin(); it2 != currentUsers.end(); ++it2)
				{
					users.push_back(it2->getUsername());
				}
			}
		}
		it = map.find(m_room.getRoomData().id);
		if (!roomClosed && !gameStarted)
		{
			if (it == map.end())
			{
				roomClosed = true;
				GetRoomStateResponse response{ 0, m_room.getRoomData().isActive, users, m_room.getRoomData().questionCount, m_room.getRoomData().timePerQuestion, m_room.getRoomData().maxPlayers };
				vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
				string mess(buffer.begin(), buffer.end());
				cout << "(" << clientSocket << ") Response: " << mess << endl;
				send(clientSocket, mess.c_str(), mess.size(), 0);
			}
			else if (Loggedusers != users)
			{
				GetRoomStateResponse response{ 1, it->second.getRoomData().isActive, users, m_room.getRoomData().questionCount, m_room.getRoomData().timePerQuestion, m_room.getRoomData().maxPlayers };
				vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
				string mess(buffer.begin(), buffer.end());
				cout << "(" << clientSocket << ") Response: " << mess << endl;
				send(clientSocket, mess.c_str(), mess.size(), 0);
				Loggedusers = users;
			}
			else if (it->second.getRoomData().isActive == true)
			{
				gameStarted = true;
				m_room = it->second;
				GetRoomStateResponse response{ 1, it->second.getRoomData().isActive, users, m_room.getRoomData().questionCount, m_room.getRoomData().timePerQuestion, m_room.getRoomData().maxPlayers };
				vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
				string mess(buffer.begin(), buffer.end());
				cout << "(" << clientSocket << ") Response: " << mess << endl;
				send(clientSocket, mess.c_str(), mess.size(), 0);
				m_roomManager.deleteRoom(m_room.getRoomData().id);
			}
		}
		this_thread::sleep_for(chrono::seconds(1));
	}
}

RequestResult RoomMemberRequestHandler::getRoomState(Request request, SOCKET clientSocket)
{
	if (getRoomStateFlag)
	{
		getRoomStateFlag = false;
		thread t1(&RoomMemberRequestHandler::getRoomStateThread, this, request, clientSocket);
		t1.detach();
	}
	else
	{
		getRoomStateFlag = true;
	}
	vector<uint8_t> v;
	if (roomClosed)
	{
		IRequestHandler* handler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(m_user));
		return RequestResult{ v, handler };
	}
	else if (gameStarted)
	{
		IRequestHandler* handler = new GameRequestHandler(m_handlerFactory.createGameRequestHandler(m_user, m_room));
		RequestResult result{ v, handler };
		return result;
	}
	return RequestResult{ v, NULL };
}

void RoomMemberRequestHandler::logout()
{
	std::map<unsigned int, Room>::iterator it;
	std::map<unsigned int, Room> map(m_roomManager.getRoomMap());
	int roomId = 0;
	bool found = false;
	for (it = map.begin(); it != map.end(); ++it)
	{
		std::vector<LoggedUser> v(it->second.getAllUsers());
		for (std::vector<LoggedUser>::iterator it2 = v.begin(); it2 != v.end(); ++it2)
		{
			if (it2->getUsername() == m_user.getUsername())
			{
				roomId = it->first;
				found = true;
			}
		}
	}
	if (found)
	{
		m_roomManager.getRoomMap()[roomId].removeUser(m_user.getUsername());
	}
	m_handlerFactory.GetLoginManager().logout(m_user.getUsername());
}