#pragma once
#include "Room.h"
#include <map>


class RoomManager
{
private:
	map<unsigned int, Room> m_rooms; // unsigned int - roomID

public:
	RoomManager();
	bool createRoom(LoggedUser,RoomData);
	bool deleteRoom(unsigned int);// unsigned int - roomID
	unsigned int getRoomState(int ID);
	vector<RoomData> getRooms() const;
	map<unsigned int, Room>& getRoomMap();
};