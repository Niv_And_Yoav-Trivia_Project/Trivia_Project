#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct SignupResponse
{
	unsigned int status;
};

inline void to_json(json& j, const SignupResponse& p)
{
	j = json{ { "status", p.status } };
}

inline void from_json(const json& j, SignupResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}