#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include "RequestResult.h"
#include "RequestHandlerFactory.h"
#include "MenuRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"


class LoginRequestHandler : public IRequestHandler
{
private:
	LoginManager& m_loginManager;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult login(Request req);
	RequestResult signup(Request req);

public:
	LoginRequestHandler(LoginManager& manager, RequestHandlerFactory& factory);
	bool isRequestRelevant(Request req);
	RequestResult handleRequest(Request, SOCKET);
};
