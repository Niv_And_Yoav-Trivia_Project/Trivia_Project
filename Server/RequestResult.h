#pragma once

#include "IRequestHandler.h"

struct RequestResult
{
	vector<uint8_t> response;
	IRequestHandler* newHandler;
};
