#include "HighscoreTable.h"

HighscoreTable::HighscoreTable(IDatabase& db) : m_database(db)
{

}

vector<pair<int, string>> HighscoreTable::getHighscores()
{
	map<LoggedUser, int> temp = m_database.getHighscores();
	vector<pair<int, string>> answer;

	for (map<LoggedUser, int>::iterator it = temp.begin(); it != temp.end(); ++it)
	{
		answer.push_back(pair<int, string>(it->second, it->first.getUsername()));
	}

	return  answer;
}