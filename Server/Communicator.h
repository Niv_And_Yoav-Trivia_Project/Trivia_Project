#pragma once

#include <map>
#include <thread>
#include <ctime>
#include "LoginRequestHandler.h"
#include "WSAInitializer.h"

class Communicator
{
private:
	map<SOCKET, IRequestHandler*> m_clients;
	SOCKET _serverSocket;
	RequestHandlerFactory& m_handlerFactory;

	void startThreadForNewClient();

public:
	Communicator(RequestHandlerFactory& factory);
	void bindAndListen();
	void handleRequests(SOCKET clientSocket);
};
