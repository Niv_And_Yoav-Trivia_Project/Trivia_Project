#include "Communicator.h"

#define PORT 5555

Communicator::Communicator(RequestHandlerFactory& factory) : m_handlerFactory(factory)
{
}

void Communicator::bindAndListen()
{
	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << PORT << endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		//cout << "Waiting for client connection request" << endl;
		startThreadForNewClient();
	}
}

void Communicator::handleRequests(SOCKET clientSocket)
{
	try
	{
		int code = 0;

		while (code != -1)
		{
			char* codeStr = new char[1];
			int res = recv(clientSocket, codeStr, 1, 0);
			if (res == INVALID_SOCKET || res == 0)
			{
				std::string s = "Error while recieving from socket: ";
				s += std::to_string(clientSocket);
				throw std::exception(s.c_str());
			}
			code = codeStr[0];


			char* len = new char[4];
			res = recv(clientSocket, len, 4, 0);
			if (res == INVALID_SOCKET || res == 0)
			{
				std::string s = "Error while recieving from socket: ";
				s += std::to_string(clientSocket);
				throw std::exception(s.c_str());
			}
			string lengthStr;
			for (int i = 0; i < 4; i++)
			{
				lengthStr += len[i];
			}
			int length;
			memcpy(&length, len, sizeof len);

			string dataStr;
			if (length > 0)
			{
				char* data = new char[length];
				res = recv(clientSocket, data, length, 0);
				if (res == INVALID_SOCKET || res == 0)
				{
					std::string s = "Error while recieving from socket: ";
					s += std::to_string(clientSocket);
					throw std::exception(s.c_str());
				}
				for (int i = 0; i < length; i++)
				{
					dataStr += data[i];
				}
			}

			string buff = "";
			buff += codeStr[0];
			buff += lengthStr;
			buff += dataStr;

			cout << (int)buff[0] << endl;

			cout << "(" << clientSocket << ") Request: " << buff << endl;

			std::vector<uint8_t> buffer(buff.begin(), buff.end());

			// current date/time based on current system
			time_t now = time(0);

			Request req {buffer[0], now, buffer};
			
			if (m_clients[clientSocket]->isRequestRelevant(req))
			{
				RequestResult result(m_clients[clientSocket]->handleRequest(req, clientSocket));
				if (result.newHandler != NULL)
				{
					m_clients[clientSocket] = result.newHandler;
					if (!(result.response.empty()))
					{
						vector<uint8_t> message = result.response;
						string mess(message.begin(), message.end());
						cout << "(" << clientSocket << ") Response: " << mess << endl;
						send(clientSocket, mess.c_str(), mess.size(), 0);
					}
 				}
			}
			else
			{
				ErrorResponse response{ "Request isnt relevant" };
				vector<uint8_t> message = JsonResponsePacketSerializer::serializeResponse(response);
				
				string mess(message.begin(), message.end());
				cout << "(" << clientSocket << ") Response: " << mess << endl;
				send(clientSocket, mess.c_str(), mess.size(), 0);
			}
		}
	}
	catch (const std::exception& e)
	{
		m_clients[clientSocket]->logout();
		cout << m_clients[clientSocket] << " logged out" << endl;
		map<SOCKET, IRequestHandler*>::iterator it;
		it = m_clients.find(clientSocket);
		m_clients.erase(it);
		closesocket(clientSocket);
	}
}

void Communicator::startThreadForNewClient()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;
	IRequestHandler* handler = new LoginRequestHandler(m_handlerFactory.createLoginRequestHandler());
	m_clients[client_socket] = handler;

	// the function that handle the conversation with the client
	thread t1(&Communicator::handleRequests, this, client_socket);
	t1.detach();
}