#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct CreateRoomResponse
{
	unsigned int status;
};

inline void to_json(json& j, const CreateRoomResponse& p)
{
	j = json{ { "status", p.status }};
}

inline void from_json(const json& j, CreateRoomResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}