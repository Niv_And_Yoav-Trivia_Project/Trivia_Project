#include "SqliteDatabase.h"
#include <utility>
#include <map>

int countUser(void *data, int argc, char **argv, char **azColName)
{
	if ('0' == argv[0][0])
		*(int*)data = 0;
	return 0;
}
int deleteUser(void *data, int argc, char **argv, char **azColName)
{
	string username;
	string password;
	string email;

	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("username" == str)
			username = argv[i];
		if ("password" == str)
			password = argv[i];
		if ("email" == str)
			email = argv[i];
	}

	LoggedUser u(username);
	((list<LoggedUser>*)data)->remove(u);
	return 0;
}

int deleteQuestion(void *data, int argc, char **argv, char **azColName)
{
	string question;
	string correct_ans;
	string ans2;
	string ans3;
	string ans4;

	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("question" == str)
			question = argv[i];
		if ("correct_ans" == str)
			correct_ans = argv[i];
		if ("ans2" == str)
			ans2 = argv[i];
		if ("ans3" == str)
			ans3 = argv[i];
		if ("ans4" == str)
			ans4 = argv[i];
	}

	Question q(question, correct_ans, ans2, ans3, ans4);
	((list<Question>*)data)->remove(q);
	return 0;
}

int deleteScore(void *data, int argc, char **argv, char **azColName)
{
	int scoreID;
	string username;
	int numOfGames;
	int rightAnswers;
	int wrongAnswers;
	double avgTimeForQuestion;

	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("scoreID" == str)
			scoreID = stoi(argv[i]);
		if ("numOfGames" == str)
			numOfGames = stoi(argv[i]);
		if ("username" == str)
			username = argv[i];
		if ("rightAnswers" == str)
			rightAnswers = stoi(argv[i]);
		if ("wrongAnswers" == str)
			wrongAnswers = stoi(argv[i]);
		if ("avgTimeForQuestion" == str)
			avgTimeForQuestion = atof(argv[i]);
	}

	PlayerScore p(scoreID, numOfGames, username, rightAnswers, wrongAnswers, avgTimeForQuestion);
	((list<PlayerScore>*)data)->remove(p);
	return 0;
}
int fillUsers(void *data, int argc, char **argv, char **azColName)
{
	string username;
	string password;
	string email;

	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("username" == str)
			username = argv[i];
		if ("password" == str)
			password = argv[i];
		if ("email" == str)
			email = argv[i];
	}

	User u(username, password, email);
	((list<User>*)data)->push_back(u);
	return 0;
}

int fillQuestions(void *data, int argc, char **argv, char **azColName)
{
	string question;
	string correct_ans;
	string ans2;
	string ans3;
	string ans4;

	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("question" == str)
			question = argv[i];
		if ("correct_ans" == str)
			correct_ans = argv[i];
		if ("ans2" == str)
			ans2 = argv[i];
		if ("ans3" == str)
			ans3 = argv[i];
		if ("ans4" == str)
			ans4 = argv[i];
	}

	Question q(question, correct_ans, ans2, ans3, ans4);
	((list<Question>*)data)->push_back(q);
	return 0;
}
int fillScores(void *data, int argc, char **argv, char **azColName)
{
	int scoreID;
	string username;
	int numOfGames;
	int rightAnswers;
	int wrongAnswers;
	double avgTimeForQuestion;

	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("scoreID" == str)
			scoreID = stoi(argv[i]);
		if ("numOfGames" == str)
			numOfGames = stoi(argv[i]);
		if ("username" == str)
			username = argv[i];
		if ("rightAnswers" == str)
			rightAnswers = stoi(argv[i]);
		if ("wrongAnswers" == str)
			wrongAnswers = stoi(argv[i]);
		if ("avgTimeForQuestion" == str)
			avgTimeForQuestion = atof(argv[i]);
	}

	PlayerScore p(scoreID, numOfGames, username, rightAnswers, wrongAnswers, avgTimeForQuestion);
	((list<PlayerScore>*)data)->push_back(p);
	return 0;
}

int updateScore(void *data, int argc, char **argv, char **azColName)
{
	int scoreID;
	string username;
	int numOfGames;
	int rightAnswers;
	int wrongAnswers;
	double avgTimeForQuestion;

	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("scoreID" == str)
			scoreID = stoi(argv[i]);
		if ("numOfGames" == str)
			numOfGames = stoi(argv[i]);
		if ("username" == str)
			username = argv[i];
		if ("rightAnswers" == str)
			rightAnswers = stoi(argv[i]);
		if ("wrongAnswers" == str)
			wrongAnswers = stoi(argv[i]);
		if ("avgTimeForQuestion" == str)
			avgTimeForQuestion = atof(argv[i]);
	}

	PlayerScore p(scoreID, numOfGames, username, rightAnswers, wrongAnswers, avgTimeForQuestion);
	*(PlayerScore*)data = p;
	return 0;
}



SqliteDatabase::SqliteDatabase()
{
	int doesFileExist = _access(_dbFileName.c_str(), 0);
	int res = sqlite3_open(_dbFileName.c_str(), &_db);
	if (res != SQLITE_OK)
	{
		_db = nullptr;
		throw exception("Failed to open DB");
	}
	if (doesFileExist != 0)
	{
		createTables();
	}

	string sqlStatement = "SELECT * FROM User;";
	char** errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), fillUsers, &m_Users, errMessage);

	sqlStatement = "SELECT * FROM Question;";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), fillQuestions, &m_Questions, errMessage);

	sqlStatement = "SELECT * FROM Score;";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), fillScores, &m_Scores, errMessage);
}
void SqliteDatabase::createTables()
{
	string sqlStatement = "CREATE TABLE User (username TEXT PRIMARY KEY NOT NULL, password TEXT NOT NULL, email TEXT NOT NULL); ";
	char **errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		throw exception("Create User table fail.");

	sqlStatement = "CREATE TABLE Question (question_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, question TEXT NOT NULL, correct_ans TEXT NOT NULL, ans2 TEXT NOT NULL, ans3 TEXT NOT NULL, ans4 TEXT NOT NULL); ";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		throw exception("Create Question table fail.");

	sqlStatement = "CREATE TABLE Score (scoreID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, numOfGames INTEGER NOT NULL, username TEXT NOT NULL, rightAnswers INTEGER NOT NULL, wrongAnswers INTEGER NOT NULL, avgTimeForQuestion DOUBLE NOT NULL);";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		throw exception("Create Score table fail.");

}

void SqliteDatabase::addUser(string username, string password, string email)
{
	string sqlStatement = "INSERT INTO User VALUES ('" + username + "','" + password + "','" + email + "');";
	char **errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		throw exception("Add User fail.");
	LoggedUser l(username);
	m_Users.push_back(l);
}


void SqliteDatabase::addQuestion(string question, string correct_ans, string ans2, string ans3, string ans4)
{
	int answer = true;
	string sqlStatement = "SELECT count(question) From Question where question == '" + question + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), countUser, &answer, &errMessage);

	if (!answer)
	{
		string sqlStatement = "INSERT INTO Question (question, correct_ans, ans2, ans3, ans4) VALUES ('" + question + "','" + correct_ans + "','" + ans2 + "','" + ans3 + "','" + ans4 + "');";
		char **errMessage = nullptr;
		int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			throw exception("Add Question fail.");
		Question q(question, correct_ans, ans2, ans3, ans4);
		m_Questions.push_back(q);
	}

}

void SqliteDatabase::addScore(int numOfGames, string username, int rightAnswers, int wrongAnswers, double avgTimeForQuestion)
{
	int answer = true;
	PlayerScore temp;
	string sqlStatement = "SELECT count(username) From Score where username == '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), countUser, &answer, &errMessage);

	if (answer)//user already exists
	{
		removeScore(username);
	}
	sqlStatement = "INSERT INTO Score (numOfGames, rightAnswers, username, wrongAnswers, avgTimeForQuestion) VALUES (" + to_string(numOfGames) + "," + to_string(rightAnswers) + ",'" + username + "'," + to_string(wrongAnswers) + "," + to_string(avgTimeForQuestion) + ");";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		throw exception("Add Score fail.");
	PlayerScore p(m_Scores.size() + 1, numOfGames, username, rightAnswers, wrongAnswers, avgTimeForQuestion);
	m_Scores.push_back(p);
}

void SqliteDatabase::removeUser(string username)
{
	string sqlStatement = "DELETE from User WHERE username == '" + username + "';";
	char **errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		throw exception("DELETE User fail.");

	sqlStatement = "SELECT * FROM User WHERE username == '" + username + "';";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), deleteUser, &m_Users, errMessage);

}

void SqliteDatabase::removeQuestion(string question)
{
	string sqlStatement = "DELETE from Question WHERE question == '" + question + "';";
	char **errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		throw exception("DELETE User fail.");

	sqlStatement = "SELECT * FROM Question WHERE question == '" + question + "';";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), deleteQuestion, &m_Questions, errMessage);
}

void SqliteDatabase::removeScore(string username)
{
	string sqlStatement = "DELETE from Score WHERE username == '" + username + "';";
	char **errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		throw exception("DELETE Score fail.");

	sqlStatement = "SELECT * FROM Score WHERE username == '" + username + "';";
	errMessage = nullptr;
	res = sqlite3_exec(_db, sqlStatement.c_str(), deleteScore, &m_Scores, errMessage);
}

list<Question> SqliteDatabase::getQuestions()
{
	return m_Questions;
}
list<LoggedUser> SqliteDatabase::getUsers()
{
	return m_Users;
}
list<PlayerScore> SqliteDatabase::getScores()
{
	return m_Scores;
}

map<LoggedUser, int> HightScoreAnswer;
map<int, LoggedUser> temp;

int getHightScoreUsers(void *data, int argc, char **argv, char **azColName)
{
	string username;
	int score;

	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("username" == str)
			username = argv[i];
		if ("rightAnswers" == str)
			score = stoi(argv[i]);
	}

	HightScoreAnswer.insert(pair<LoggedUser, int>(LoggedUser(username), score));
	return 0;
}

map<LoggedUser, int> SqliteDatabase::getHighscores()
{
	HightScoreAnswer.clear();
	string sqlStatement = "SELECT * FROM Score ORDER BY rightAnswers DESC LIMIT 3;";
	char **errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), getHightScoreUsers, nullptr, errMessage);
	if (res != SQLITE_OK)
		throw exception("getHighscores fail.");
	return HightScoreAnswer;
}
bool SqliteDatabase::doesUsernameExist(string username)
{
	int answer = true;
	string sqlStatement = "SELECT count(username) From User where username == '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), countUser, &answer, &errMessage);
	return answer;
}

bool SqliteDatabase::doesUsernameAndPasswordExist(string username, string password)
{
	int answer = true;
	if (doesUsernameExist(username))
	{
		string sqlStatement = "SELECT count(username) From User where username == '" + username + "' AND password == '" + password + "';";
		char** errMessage = nullptr;
		int res = sqlite3_exec(_db, sqlStatement.c_str(), countUser, &answer, errMessage);
		return answer;
	}
	return false;
}

int findEmail(void *data, int argc, char **argv, char **azColName)
{
	for (int i = 0; i < argc; i++)
	{
		string str(azColName[i]);
		if ("email" == str)
			*(string*)data = argv[i];
	}

	return 0;
}

LoggedUser SqliteDatabase::findUser(string username, string password)
{
	string email;
	if (doesUsernameAndPasswordExist(username, password))
	{
		string sqlStatement = "SELECT email From User where password == '" + password + "';";
		char** errMessage = nullptr;
		int res = sqlite3_exec(_db, sqlStatement.c_str(), findEmail, &email, errMessage);
	}

	LoggedUser l(username);
	return l;
}


PlayerScore SqliteDatabase::getScoreByUsername(string username)
{
	int answer = true;
	string sqlStatement = "SELECT count(username) From Score where username == '" + username + "';";
	char* errMessage = nullptr;

	int res = sqlite3_exec(_db, sqlStatement.c_str(), countUser, &answer, &errMessage);

	if (answer)//user already exists
	{
		PlayerScore temp;
		sqlStatement = "SELECT * From Score where username == '" + username + "';";
		char* errMessage = nullptr;
		int res = sqlite3_exec(_db, sqlStatement.c_str(), updateScore, &temp, &errMessage);
		return temp;
	}
	else
	{
		PlayerScore temp(0, 0, username, 0, 0, 0);
		return temp;
	}
}