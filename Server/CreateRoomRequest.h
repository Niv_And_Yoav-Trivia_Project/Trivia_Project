#pragma once

#include <iostream>
#include <string>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct CreateRoomRequest
{
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

inline void to_json(json& j, const CreateRoomRequest& p)
{
	j = json{ { "roomName", p.roomName },{ "maxUsers", p.maxUsers }, { "questionCount", p.questionCount }, { "answerTimeout", p.answerTimeout } };
}

inline void from_json(const json& j, CreateRoomRequest& p)
{
	p.roomName = j.at("roomName").get<string>();
	p.maxUsers = j.at("maxUsers").get<unsigned int>();
	p.questionCount = j.at("questionCount").get<unsigned int>();
	p.answerTimeout = j.at("answerTimeout").get<unsigned int>();
}