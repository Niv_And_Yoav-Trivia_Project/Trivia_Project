#pragma once

#include <vector>
#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct HighscoreResponse
{
	unsigned int status;
	vector<pair<int, string>> highscores;
};

inline void to_json(json& j, const HighscoreResponse& p)
{
	j = json{ { "status", p.status },{ "highscores", p.highscores } };
}

inline void from_json(const json& j, HighscoreResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
	p.highscores = j.at("highscores").get<vector<pair<int, string>>>();
}