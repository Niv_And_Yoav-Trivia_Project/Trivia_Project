#include "GameManager.h"

GameManager::GameManager(IDatabase& db) : m_database(db)
{
}

int myrandom(int i) { return std::rand() % i; }

Game* GameManager::createGame(Room room)
{
	vector<LoggedUser> users(room.getAllUsers());
	map<LoggedUser, GameData> players;
	list<Question> questionsList(m_database.getQuestions());
	vector<Question> questions{ make_move_iterator(begin(questionsList)), make_move_iterator(end(questionsList)) };
	srand(time(NULL));
	random_shuffle(questions.begin(), questions.end());
	for (vector<LoggedUser>::iterator it = users.begin(); it != users.end(); ++it)
	{
		if (questions.size() != 0)
		{
			GameData data{ questions[0], 0, 0, 0, false };
			players.insert(pair<LoggedUser, GameData>(*it, data));
		}
		else
		{
			Question q("", "", "", "", "");
			GameData data{ q, 0, 0, 0, false };
			players.insert(pair<LoggedUser, GameData>(*it, data));
		}
	}
	Game* game = new Game(questions, players);
	m_games.push_back(game);
	return game;
}

void GameManager::deleteGame(Game* game)
{
	vector<Game*>::iterator position = find(m_games.begin(), m_games.end(), game);

	if (position != m_games.end())
	{
		m_games.erase(position);

		map<LoggedUser, GameData> players(game->getPlayers());
		for (map<LoggedUser, GameData>::const_iterator it = players.begin(); it != players.end(); ++it)
		{
			PlayerScore score(m_database.getScoreByUsername(it->first.getUsername()));
			m_database.addScore((score.numOfGames + 1), it->first.getUsername(), (score.rightAnswers + it->second.correctAnswerCount), (score.wrongAnswers + it->second.wrongAnswerCount), (((score.numOfGames * score.avgTimeForQuestion) + it->second.averageAnswerTime) / (score.numOfGames + 1)));
		}
	}
}

vector<Game*>& GameManager::getGames()
{
	return m_games;
}