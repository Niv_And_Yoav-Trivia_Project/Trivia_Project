#pragma once
#include <iostream>
#include <algorithm>
#include "Game.h"
#include "Room.h"
#include "SqliteDatabase.h"

using namespace std;

class GameManager
{
private:
	IDatabase& m_database;
	vector<Game*> m_games;

public:
	GameManager(IDatabase& db);
	Game* createGame(Room);
	void deleteGame(Game*);

	vector<Game*>& getGames();
};