#pragma once

#include <iostream>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

inline void to_json(json& j, const GetPlayersInRoomRequest& p)
{
	j = json{ { "roomId", p.roomId }};
}

inline void from_json(const json& j, GetPlayersInRoomRequest& p)
{
	p.roomId = j.at("roomId").get<unsigned int>();
}