#include "RoomManager.h"

RoomManager::RoomManager()
{
}

vector<RoomData> RoomManager::getRooms() const
{
	vector<RoomData> temp;

	for (auto it = m_rooms.cbegin(); it != m_rooms.cend(); ++it)
	{
		temp.push_back(it->second.getRoomData());
	}

	return temp;
}

unsigned int RoomManager::getRoomState(int ID)
{
	for (auto it = m_rooms.cbegin(); it != m_rooms.cend(); ++it)
	{
		if (it->first == ID)
		{
			return it->second.getRoomData().isActive;
		}
	}
}

bool RoomManager::createRoom(LoggedUser user, RoomData RD)
{
	std::map<unsigned int, Room>::iterator it;
	for (it = m_rooms.begin(); it != m_rooms.end(); ++it)
		if (it->second.getRoomData().name == RD.name)
			return false;
	Room r(RD, user);
	m_rooms[RD.id] = r;
	return true;
}

bool RoomManager::deleteRoom(unsigned int ID)// unsigned int - roomID
{
	map<unsigned int, Room>::iterator it;

	it = m_rooms.find(ID);
	if (it != m_rooms.end())
	{
		m_rooms.erase(it);
		return true;
	}
	else
	{
		return false;
	}
}

map<unsigned int, Room>& RoomManager::getRoomMap()
{
	return m_rooms;
}