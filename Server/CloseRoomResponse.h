#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct CloseRoomResponse
{
	unsigned int status;
};

inline void to_json(json& j, const CloseRoomResponse& p)
{
	j = json{ { "status", p.status } };
}

inline void from_json(const json& j, CloseRoomResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}