#pragma once

#include <iostream>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct JoinRoomRequest
{
	unsigned int roomId;
};

inline void to_json(json& j, const JoinRoomRequest& p)
{
	j = json{ { "roomId", p.roomId } };
}

inline void from_json(const json& j, JoinRoomRequest& p)
{
	p.roomId = j.at("roomId").get<unsigned int>();
}