#pragma once

#include <iostream>
#include <vector>
#include <string>
#include "json.hpp"

using nlohmann::json;
using namespace std;

struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	vector<string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
	unsigned int maxPlayers;
};

inline void to_json(json& j, const GetRoomStateResponse& p)
{
	j = json{ { "status", p.status },{ "hasGameBegun", p.hasGameBegun },{ "players", p.players },{ "questionCount", p.questionCount },{ "answerTimeout", p.answerTimeout },{ "maxPlayers", p.maxPlayers } };
}

inline void from_json(const json& j, GetRoomStateResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
	p.hasGameBegun = j.at("hasGameBegun").get<bool>();
	p.players = j.at("players").get<vector<string>>();
	p.questionCount = j.at("questionCount").get<unsigned int>();
	p.answerTimeout = j.at("answerTimeout").get<unsigned int>();
	p.maxPlayers	 = j.at("maxPlayers").get<unsigned int>();
}