#pragma once
#include <iostream>

using namespace std;

class PlayerScore
{
public:
	int scoreID;
	string username;
	int numOfGames;
	int rightAnswers;
	int wrongAnswers;
	double avgTimeForQuestion;

	PlayerScore();
	PlayerScore(int scoreID, int numOfGames, string username, int rightAnswers, int wrongAnswers, double avgTimeForQuestion);
	bool operator==(const PlayerScore& other);
	void operator=(const PlayerScore& other);
};