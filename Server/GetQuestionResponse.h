#pragma once

#include <iostream>
#include <string>
#include <map>
#include "json.hpp"

using nlohmann::json;
using namespace std;

struct GetQuestionResponse
{
	unsigned int status;
	string question;
	vector<string> answers;
};

inline void to_json(json& j, const GetQuestionResponse& p)
{
	j = json{ { "status", p.status }, { "question", p.question }, { "answers", p.answers } };
}

inline void from_json(const json& j, GetQuestionResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
	p.question = j.at("question").get<string>();
	p.answers = j.at("answers").get<vector<string>>();
}