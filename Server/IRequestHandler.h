#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "Request.h"

using namespace std;

struct RequestResult;

class IRequestHandler	
{
public:
	bool virtual isRequestRelevant(Request req) = 0;
	virtual RequestResult handleRequest(Request, SOCKET) = 0;
	virtual void logout()
	{
	}
};