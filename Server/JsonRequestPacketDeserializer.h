#pragma once

#include "json.hpp"
#include <vector>
#include <iostream>
#include "LoginRequest.h"
#include "SignupRequest.h"
#include "GetPlayersInRoomRequest.h"
#include "JoinRoomRequest.h"
#include "CreateRoomRequest.h"
#include "SubmitAnswerRequest.h"
#include "GetMyStatusRequest.h"

using json = nlohmann::json;

using namespace std;

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(vector<uint8_t> buffer);
	static SignupRequest deserializeSignupRequest(vector<uint8_t> buffer);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(vector<uint8_t> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(vector<uint8_t> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(vector<uint8_t> buffer);

	static SubmitAnswerRequest deserializeSubmitAnswerRequest(vector<uint8_t> buffer);

	static GetMyStatusRequest deserializeGetMyStatusRequest(vector<uint8_t> buffer);
};