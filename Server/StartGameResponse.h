#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct StartGameResponse
{
	unsigned int status;
};

inline void to_json(json& j, const StartGameResponse& p)
{
	j = json{ { "status", p.status } };
}

inline void from_json(const json& j, StartGameResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}