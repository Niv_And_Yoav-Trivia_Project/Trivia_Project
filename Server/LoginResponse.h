#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct LoginResponse
{
	unsigned int status;
};

inline void to_json(json& j, const LoginResponse& p)
{
	j = json{ { "status", p.status } };
}

inline void from_json(const json& j, LoginResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}