﻿#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(LoggedUser user, RoomManager& manager, HighscoreTable& table, RequestHandlerFactory& factory) : m_user(user), m_roomManager(manager), m_highscoreTable(table), m_handlerFactory(factory)
{
	getRoomsFlag = true;
}

bool MenuRequestHandler::isRequestRelevant(Request req)
{
	int code = (int)req.buffer[0];
	if (code >= LOGOUT_CODE && code <= GET_HIGHSCORE_CODE || code == GET_STATUS_CODE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

RequestResult MenuRequestHandler::handleRequest(Request request, SOCKET clientSocket)
{
	switch (request.id)
	{
	case LOGOUT_CODE:
		return signout(request);
		break;
	case GET_ROOMS_CODE:
		return getRooms(request, clientSocket);
		break;
	case GET_PLAYERS_IN_ROOM_CODE:
		return getPlayersInRoom(request);
		break;
	case JOIN_ROOM_CODE:
		return joinRoom(request);
		break;
	case CREATE_ROOM_CODE:
		return createRoom(request);
		break;
	case GET_HIGHSCORE_CODE:
		return getHighscores(request);
		break;
	case GET_STATUS_CODE:
		return myStatus(request);
		break;
	default:
		ErrorResponse response{ "Code doesnt exist" };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
		break;
	}
}

RequestResult MenuRequestHandler::createRoom(Request request)
{
	CreateRoomRequest createRequest(JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request.buffer));
	RoomData data{ MenuRequestHandler::roomCount, createRequest.roomName, createRequest.maxUsers, createRequest.answerTimeout, false , createRequest.questionCount };
	int roomCount = MenuRequestHandler::roomCount;
	MenuRequestHandler::roomCount++;
	if (m_roomManager.createRoom(m_user, data))
	{
		CreateRoomResponse response{ SUCCEEDED };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		Room r(data, m_user);
		IRequestHandler* handler = new RoomAdminRequestHandler(m_handlerFactory.createRoomAdminRequestHandler(m_user, r));
		RequestResult result{ buffer, handler };
		return result;
	}
	else
	{
		CreateRoomResponse response{ ROOM_NAME_ALREADY_EXIST };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
}

RequestResult MenuRequestHandler::getPlayersInRoom(Request request)
{
	GetPlayersInRoomRequest getPlayersRequest(JsonRequestPacketDeserializer::deserializeGetPlayersRequest(request.buffer));
	std::map<unsigned int, Room>::iterator it;
	vector<string> users;
	std::map<unsigned int, Room> map(m_roomManager.getRoomMap());
	for (it = map.begin(); it != map.end(); ++it)
	{
		if (it->first == getPlayersRequest.roomId)
		{
			std::vector<LoggedUser> v(it->second.getAllUsers());
			for (std::vector<LoggedUser>::iterator it2 = v.begin(); it2 != v.end(); ++it2)
			{
				users.push_back(it2->getUsername());
			}
		}
	}
	GetPlayersInRoomResponse response { users };
	vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	RequestResult result{ buffer, this };
	return result;
}

RequestResult MenuRequestHandler::joinRoom(Request request)
{
	JoinRoomRequest joinRequest(JsonRequestPacketDeserializer::deserializeJoinRoomRequest(request.buffer));
	bool added = false;
	std::map<unsigned int, Room>::iterator it;
	std::map<unsigned int, Room> map(m_roomManager.getRoomMap());
	for (it = map.begin(); it != map.end(); ++it)
	{
		if (it->first == joinRequest.roomId)
		{
			if (it->second.getRoomData().maxPlayers != it->second.getAllUsers().size())
			{
				added = true;
			}
		}
	}
	if (added)
	{
		m_roomManager.getRoomMap()[joinRequest.roomId].addUser(m_user);
		JoinRoomResponse response{ SUCCEEDED };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		Room r(m_roomManager.getRoomMap()[joinRequest.roomId].getRoomData(), m_user);
		IRequestHandler* handler = new RoomMemberRequestHandler(m_handlerFactory.createRoomMemberRequestHandler(m_user, r));
		RequestResult result{ buffer, handler };
		return result;
	}
	else
	{
		JoinRoomResponse response{ ROOM_DOESNT_EXIST };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
}

void MenuRequestHandler::getRoomsThread(Request request, SOCKET clientSocket)
{
	vector<RoomData> rooms;
	while (!getRoomsFlag)
	{
		vector<RoomData> currentRooms;
		std::map<unsigned int, Room>::iterator it;
		std::map<unsigned int, Room> map(m_roomManager.getRoomMap());
		for (it = map.begin(); it != map.end(); ++it)
		{
			if (it->second.getRoomData().isActive == false)
			{
				currentRooms.push_back(it->second.getRoomData());
			}
		}

		if (rooms.size() != currentRooms.size())
		{
			GetRoomsResponse response{ 1, currentRooms };
			vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
			string mess(buffer.begin(), buffer.end());
			cout << "(" << clientSocket << ") Response: " << mess << endl;
			send(clientSocket, mess.c_str(), mess.size(), 0);
			rooms = currentRooms;
		}
		else
		{
			bool equal = true;
			for (int i = 0; i < currentRooms.size(); i++)
			{
				string name = currentRooms[i].name;
				bool found = false;
				for (int j = 0; j < rooms.size(); j++)
				{
					if (rooms[j].name == name)
					{
						found = true;
					}
				}
				if (!found)
				{
					equal = false;
				}
			}

			if (!equal)
			{
				GetRoomsResponse response{ 1, currentRooms };
				vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
				string mess(buffer.begin(), buffer.end());
				cout << "(" << clientSocket << ") Response: " << mess << endl;
				send(clientSocket, mess.c_str(), mess.size(), 0);
				rooms = currentRooms;
			}
		}
		this_thread::sleep_for(chrono::seconds(1));
	}
}

RequestResult MenuRequestHandler::getRooms(Request request, SOCKET clientSocket)
{
	if (getRoomsFlag)
	{
		getRoomsFlag = false;
		thread t1(&MenuRequestHandler::getRoomsThread, this, request, clientSocket);
		t1.detach();
	}
	else
	{
		getRoomsFlag = true;
	}
	vector<uint8_t> v;
	return RequestResult{v, NULL};
}

RequestResult MenuRequestHandler::getHighscores(Request)
{
	vector<pair<int, string>> highscores = m_highscoreTable.getHighscores();

	sort(highscores.begin(), highscores.end());

	HighscoreResponse response{ SUCCEEDED, highscores };
	vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	RequestResult result{ buffer, this };
	return result;
}

RequestResult MenuRequestHandler::myStatus(Request request)
{
	GetMyStatusRequest statusRequest(JsonRequestPacketDeserializer::deserializeGetMyStatusRequest(request.buffer));
	PlayerScore score(m_handlerFactory.GetLoginManager().GetDataBase().getScoreByUsername(statusRequest.username));
	if (score.scoreID != 0)
	{
		GetMyStatusResponse response{ score.numOfGames, score.rightAnswers, score.wrongAnswers, score.avgTimeForQuestion };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
	else
	{
		GetMyStatusResponse response{ score.numOfGames, score.rightAnswers, score.wrongAnswers, score.avgTimeForQuestion };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
}

RequestResult MenuRequestHandler::signout(Request request)
{
	if (m_handlerFactory.GetLoginManager().logout(m_user.getUsername()))
	{
		LogoutResponse response{ SUCCEEDED };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		IRequestHandler* handler = new LoginRequestHandler(m_handlerFactory.createLoginRequestHandler());
		RequestResult result{ buffer, handler };//temp - other page
		return result;
	}
	else
	{
		LogoutResponse response{ UNABLE_TO_SIGNOUT };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
}

void MenuRequestHandler::logout()
{
	m_handlerFactory.GetLoginManager().logout(m_user.getUsername());
}