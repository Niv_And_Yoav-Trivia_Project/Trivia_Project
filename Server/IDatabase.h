#pragma once
#include <list>
#include <iostream>
#include <io.h>
#include <string>
#include "sqlite3.h"
#include "Question.h"
#include "Game.h"
#include "User.h"
#include <map>
#include <utility>      // std::pair, std::make_pair
#include "LoggedUser.h"
#include "PlayerScore.h"

using namespace std;

class IDatabase
{
protected:
	sqlite3 * _db;
	string _dbFileName = "triviaDB.sqlite";
	list<Question> m_Questions;
	list<LoggedUser> m_Users;
	list<PlayerScore> m_Scores;

public:
	virtual void createTables() = 0;

	virtual	void addUser(string username, string password, string email) = 0;
	virtual void addQuestion(string question, string correct_ans, string ans2, string ans3, string ans4) = 0;
	virtual void addScore(int numOfGames, string username, int rightAnswers, int wrongAnswers, double avgTimeForQuestion) = 0;


	virtual	void removeUser(string username) = 0;
	virtual void removeQuestion(string question) = 0;
	virtual void removeScore(string username) = 0;

	virtual list<Question> getQuestions() = 0;
	virtual list<LoggedUser> getUsers() = 0;
	virtual list<PlayerScore> getScores() = 0;


	virtual map<LoggedUser, int> getHighscores() = 0;
	virtual bool doesUsernameExist(string) = 0;
	virtual bool doesUsernameAndPasswordExist(string, string) = 0;
	virtual LoggedUser findUser(string, string) = 0;
	virtual PlayerScore getScoreByUsername(string) = 0;
};