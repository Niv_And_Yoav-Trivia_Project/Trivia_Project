#pragma once

#include <iostream>
#include <string>
#include <map>
#include "json.hpp"

using nlohmann::json;
using namespace std;

struct SubmitAnswerResponse
{
	unsigned int status;
};

inline void to_json(json& j, const SubmitAnswerResponse& p)
{
	j = json{ { "status", p.status } };
}

inline void from_json(const json& j, SubmitAnswerResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}