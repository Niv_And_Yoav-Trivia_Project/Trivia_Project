#pragma once

#include <iostream>
#include <string>
#include <map>
#include "json.hpp"
#include "PlayerResults.h"

using nlohmann::json;
using namespace std;

struct GetGameResultsResponse
{
	unsigned int status;
	vector<PlayerResults> results;
};

inline void to_json(json& j, const GetGameResultsResponse& p)
{
	j = json{ { "status", p.status },{ "results", p.results } };
}

inline void from_json(const json& j, GetGameResultsResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
	p.results = j.at("results").get<vector<PlayerResults>>();
}