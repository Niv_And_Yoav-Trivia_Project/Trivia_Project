#include "LoginManager.h"

LoginManager::LoginManager(IDatabase& db) : m_database(db)
{
}

bool LoginManager::signup(string username, string password, string email)
{
	if (!m_database.doesUsernameExist(username))
	{
		m_database.addUser(username, password, email);//add the user to the database
		return true;
	}
	//return true if succeed to signup
	return false;//temp
}

int LoginManager::login(string username, string password)
{
	for (std::vector<LoggedUser>::iterator it = m_loggedUsers.begin(); it != m_loggedUsers.end(); ++it)
	{
		if (it->getUsername() == username)
		{
			return USERNAME_ALREADY_EXIST;
		}
	}
	if (m_database.doesUsernameAndPasswordExist(username, password))
	{
		m_loggedUsers.push_back(LoggedUser(username));
		return SUCCEEDED;
	}
	return 0;
}

bool LoginManager::logout(string username)
{
	for (int i = 0; i < m_loggedUsers.size(); i++)
	{
		if (m_loggedUsers[i].getUsername() == username)
		{
			m_loggedUsers.erase(m_loggedUsers.begin() + i);
			return true;
		}
	}
	return false;
}

IDatabase& LoginManager::GetDataBase() const
{
	return m_database;
}

void LoginManager::SetDataBase(IDatabase& db)
{
	m_database = db;
}

void LoginManager::SetLoggedUsers(vector<LoggedUser> loggedUsers)
{
	m_loggedUsers = loggedUsers;
}

vector<LoggedUser> LoginManager::GetLoggedUsers() const
{
	return m_loggedUsers;
}

void LoginManager::operator=(const LoginManager& manager)
{
	this->m_database = manager.GetDataBase();
	this->m_loggedUsers = manager.GetLoggedUsers();
}