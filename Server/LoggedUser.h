#pragma once

#include <string>

using namespace std;

class LoggedUser
{
private:
	string m_username;

public:
	LoggedUser();
	LoggedUser(string user);
	string getUsername() const;
	void setUsername(string username);
	bool operator==(const LoggedUser& other);
	bool operator<(const LoggedUser& rhs) const // must be const-correct
	{
		// return true if, in a sorted sequence, the node *this should appear before the node rhs 
		// for example:
		return this->getUsername() < rhs.getUsername(); // ordered on ascending value 
	}
	bool operator>(const LoggedUser& rhs) const { return this->getUsername() > rhs.getUsername(); }
};
