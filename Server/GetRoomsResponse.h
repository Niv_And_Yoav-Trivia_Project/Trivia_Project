#pragma once

#include <vector>
#include <iostream>
#include "RoomData.h"
#include "json.hpp"

using nlohmann::json;

struct GetRoomsResponse
{
	unsigned int status;
	vector<RoomData> rooms;
};

inline void to_json(json& j, const GetRoomsResponse& p)
{
	j = json{ { "status", p.status }, { "rooms", p.rooms } };
}

inline void from_json(const json& j, GetRoomsResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
	p.rooms = j.at("rooms").get<vector<RoomData>>();
}