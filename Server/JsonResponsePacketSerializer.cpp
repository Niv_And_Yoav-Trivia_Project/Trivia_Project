#include "JsonResponsePacketSerializer.h"

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = ERROR_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = LOGIN_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = SIGNUP_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = LOGOUT_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = GET_ROOMS_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = GET_PLAYERS_IN_ROOM_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = JOIN_ROOM_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = CREATE_ROOM_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(HighscoreResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = GET_HIGHSCORE_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = CLOSE_ROOM_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(StartGameResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = START_GAME_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = GET_ROOM_STATE_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = LEAVE_ROOM_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = GET_QUESTION_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = SUBMIT_ANSWER_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = GET_GAME_RESULTS_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}

vector<uint8_t> JsonResponsePacketSerializer::serializeResponse(GetMyStatusResponse response)
{
	json j;
	j = response;
	std::string message = j.dump();

	vector<std::uint8_t> buff;
	unsigned char code = GET_STATUS_CODE;
	unsigned char len[4];
	buff.push_back(code);
	int size = message.length();
	memcpy(len, &size, sizeof size);
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(len[i]);
	}
	for (int i = 0; i < size; i++)
	{
		buff.push_back(message[i]);
	}
	return buff;
}