#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Question.h"
#include "LoggedUser.h"
#include "GameData.h"

using namespace std;

class Game
{
private:
	vector<Question> m_questions;
	map<LoggedUser, GameData> m_players;

public:
	Game(vector<Question> questions, map<LoggedUser, GameData> players);
	Question getQuestionForUser(LoggedUser);
	void submitAnswer(LoggedUser user, bool answeredCorrect, unsigned int time, int numOfQuestion);
	void removePlayer(LoggedUser);
	map<LoggedUser, GameData>& getPlayers();
	map<LoggedUser, GameData> getPlayersConst() const;
	vector<Question> getQuestions() const;

	bool operator==(const Game& other);
};