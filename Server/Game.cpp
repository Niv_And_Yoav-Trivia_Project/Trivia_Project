#include "Game.h"

Game::Game(vector<Question> questions, map<LoggedUser, GameData> players)
{
	m_questions = questions;
	m_players = players;
}

void Game::removePlayer(LoggedUser user)
{
	map<LoggedUser, GameData>::iterator it;
	map<LoggedUser, GameData>::iterator position;

	for (it = m_players.begin(); it != m_players.end(); it++)
	{
		if (it->first.getUsername() == user.getUsername())
		{
			position = it;
		}
	}

	if (position != m_players.end())
	{
		m_players.erase(position);
	}
}

void Game::submitAnswer(LoggedUser user, bool answeredCorrect, unsigned int time, int numOfQuestion)
{
	map<LoggedUser, GameData>::iterator it;
	map<LoggedUser, GameData>::iterator position;

	for (it = m_players.begin(); it != m_players.end(); it++)
	{
		if (it->first.getUsername() == user.getUsername())
		{
			position = it;
		}
	}

	if (answeredCorrect)
	{
		position->second.correctAnswerCount++;
	}
	else
	{
		position->second.wrongAnswerCount++;
	}
	position->second.averageAnswerTime += time;
	if (numOfQuestion < m_questions.size())
	{
		position->second.currentQuestion = m_questions[numOfQuestion];
	}
}

Question Game::getQuestionForUser(LoggedUser user)
{
	map<LoggedUser, GameData>::iterator it;
	map<LoggedUser, GameData>::iterator position;

	for (it = m_players.begin(); it != m_players.end(); it++)
	{
		if (it->first.getUsername() == user.getUsername())
		{
			position = it;
		}
	}

	return position->second.currentQuestion;
}

map<LoggedUser, GameData>& Game::getPlayers()
{
	return m_players;
}

map<LoggedUser, GameData> Game::getPlayersConst() const
{
	return m_players;
}

vector<Question> Game::getQuestions() const
{
	return m_questions;
}

bool Game::operator==(const Game& other)
{
	if (m_players.begin()->first.getUsername() == other.getPlayersConst().begin()->first.getUsername())
	{
		return true;
	}
	else
	{
		return false;
	}
}