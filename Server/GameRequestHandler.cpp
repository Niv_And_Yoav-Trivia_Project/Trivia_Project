#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(Game& game, LoggedUser user, GameManager& manager, RequestHandlerFactory& factory) : m_game(game), m_user(user), m_gameManager(manager), m_handlerFactory(factory)
{
	numOfQuestion = 0;
}

bool GameRequestHandler::isRequestRelevant(Request req)
{
	int code = (int)req.buffer[0];
	if (code >= GET_QUESTION_CODE && code <= LEAVE_GAME_CODE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

RequestResult GameRequestHandler::handleRequest(Request request, SOCKET)
{
	switch (request.id)
	{
	case GET_QUESTION_CODE:
		return getQuestion(request);
		break;
	case SUBMIT_ANSWER_CODE:
		return submitAnswer(request);
		break;
	case GET_GAME_RESULTS_CODE:
		return getGameResults(request);
		break;
	case LEAVE_GAME_CODE:
		return leaveGame(request);
		break;
	default:
		ErrorResponse response{ "Code doesnt exist" };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
		break;
	}
}

RequestResult GameRequestHandler::getQuestion(Request request)
{
	Question q(m_game.getQuestionForUser(m_user));
	GetQuestionResponse response;
	if (numOfQuestion < m_game.getQuestions().size())
	{
		response = { SUCCEEDED, q.getQuestion(), q.getPossibleAnswers() };
	}
	else
	{
		response = { NO_QUESTION_TO_GET, q.getQuestion(), q.getPossibleAnswers() };
	}

	vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	RequestResult result{ buffer, this };
	return result;
}

RequestResult GameRequestHandler::submitAnswer(Request request)
{
	SubmitAnswerRequest req(JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(request.buffer));
	numOfQuestion++;
	m_game.submitAnswer(m_user, req.answeredCorrect, req.answerTime, numOfQuestion);
	SubmitAnswerResponse response{ SUCCEEDED };
	vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	RequestResult result{ buffer, this };
	return result;
}

RequestResult GameRequestHandler::getGameResults(Request request)
{
	map<LoggedUser, GameData>::iterator position = m_game.getPlayers().find(m_user);
	position->second.finishedGame = true;
	position->second.averageAnswerTime /= numOfQuestion;

	bool allFinished = true;
	map<LoggedUser, GameData> m(m_game.getPlayers());
	for (map<LoggedUser, GameData>::const_iterator it = m.begin(); it != m.end(); ++it)
	{
		if (!(it->second.finishedGame))
		{
			allFinished = false;
		}
	}
	while (!allFinished)
	{
		map<LoggedUser, GameData> m(m_game.getPlayers());
		allFinished = true;
		for (map<LoggedUser, GameData>::const_iterator it = m.begin(); it != m.end(); ++it)
		{
			if (!(it->second.finishedGame))
			{
				allFinished = false;
			}
		}
	}
	vector<PlayerResults> results;
	map<LoggedUser, GameData> m2(m_game.getPlayers());
	for (map<LoggedUser, GameData>::const_iterator it = m2.begin(); it != m2.end(); ++it)
	{
		PlayerResults r{ it->first.getUsername(), it->second.correctAnswerCount, it->second.wrongAnswerCount, it->second.averageAnswerTime };
		results.push_back(r);
	}
	m_gameManager.deleteGame(&m_game);

	GetGameResultsResponse response{ SUCCEEDED, results };
	vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
	IRequestHandler* handler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(m_user));
	RequestResult result{ buffer, handler };
	return result;
}

RequestResult GameRequestHandler::leaveGame(Request request)
{
	m_game.removePlayer(m_user);
	vector<uint8_t> v;
	IRequestHandler* handler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(m_user));
	RequestResult result{ v, handler };
	return result;
}

void GameRequestHandler::logout()
{
	m_game.removePlayer(m_user);
	m_handlerFactory.GetLoginManager().logout(m_user.getUsername());
}