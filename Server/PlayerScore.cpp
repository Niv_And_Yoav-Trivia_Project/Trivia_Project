#include "PlayerScore.h"

PlayerScore::PlayerScore()
{
	this->scoreID = 0;
	this->numOfGames = 0;
	this->username = "";
	this->rightAnswers = 0;
	this->wrongAnswers = 0;
	this->avgTimeForQuestion = 0;
}

PlayerScore::PlayerScore(int scoreID, int numOfGames, string username, int rightAnswers, int wrongAnswers, double avgTimeForQuestion)
{
	this->scoreID = scoreID;
	this->numOfGames = numOfGames;
	this->username = username;
	this->rightAnswers = rightAnswers;
	this->wrongAnswers = wrongAnswers;
	this->avgTimeForQuestion = avgTimeForQuestion;
}

bool PlayerScore::operator==(const PlayerScore& other)
{
	if (this->scoreID == other.scoreID &&
		this->numOfGames == other.numOfGames &&
		this->username == other.username &&
		this->rightAnswers == other.rightAnswers&&
		this->wrongAnswers == other.wrongAnswers &&
		this->avgTimeForQuestion == other.avgTimeForQuestion)
	{
		return true;
	}
	return false;
}

void PlayerScore::operator=(const PlayerScore& other)
{
	this->scoreID = other.scoreID;
	this->numOfGames = other.numOfGames;
	this->username = other.username;
	this->rightAnswers = other.rightAnswers;
	this->wrongAnswers = other.wrongAnswers;
	this->avgTimeForQuestion = other.avgTimeForQuestion;
}