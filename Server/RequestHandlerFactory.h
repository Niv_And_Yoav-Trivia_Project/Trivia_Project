#pragma once

#include "LoginManager.h"
#include "RoomManager.h"
#include "GameManager.h"
#include "HighscoreTable.h"

class GameRequestHandler;
class LoginRequestHandler;
class MenuRequestHandler;
class RoomMemberRequestHandler;
class RoomAdminRequestHandler;

class RequestHandlerFactory
{
private:
	LoginManager& m_loginManager;
	RoomManager& m_roomManager;
	HighscoreTable& m_highscoreTable;
	GameManager& m_gameManager;

public:
	RequestHandlerFactory(LoginManager& loginManager, RoomManager& roomManager, HighscoreTable& table, GameManager& gameManager);
	LoginRequestHandler createLoginRequestHandler();
	MenuRequestHandler createMenuRequestHandler(LoggedUser user);
	RoomAdminRequestHandler createRoomAdminRequestHandler(LoggedUser user, Room room);
	RoomMemberRequestHandler createRoomMemberRequestHandler(LoggedUser user, Room room);
	GameRequestHandler createGameRequestHandler(LoggedUser user, Room room);

	LoginManager& GetLoginManager() const;

	void operator=(const RequestHandlerFactory& factory);
};