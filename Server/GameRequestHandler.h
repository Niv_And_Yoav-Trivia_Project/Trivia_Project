#pragma once
#include <iostream>
#include "Game.h"
#include "RequestHandlerFactory.h"
#include "MenuRequestHandler.h"
#include "GameManager.h"
#include "RequestResult.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

#define NO_QUESTION_TO_GET 0
#define SUCCEEDED 1

using namespace std;

class GameRequestHandler : public IRequestHandler
{
private:
	Game& m_game;
	LoggedUser m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_handlerFactory;
	int numOfQuestion;

	RequestResult getQuestion(Request);
	RequestResult submitAnswer(Request);
	RequestResult getGameResults(Request);
	RequestResult leaveGame(Request);

public:
	GameRequestHandler(Game& m_game, LoggedUser user, GameManager& manager, RequestHandlerFactory& factory);
	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request, SOCKET);
	void logout();
};