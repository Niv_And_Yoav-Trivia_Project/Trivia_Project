#pragma once
#include "RoomData.h"
#include <vector>
#include "LoggedUser.h"


class Room
{
private:
	RoomData m_metadata;
	vector<LoggedUser> m_users;

public: 
	Room();
	Room(RoomData RD, LoggedUser user);
	void addUser(LoggedUser);
	void removeUser(LoggedUser);
	vector<LoggedUser>& getAllUsers();
	RoomData getRoomData() const;
	void SetIsActive(bool isActive);
};