#pragma once

#include <iostream>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct GetMyStatusResponse
{
	int numOfGames;
	int rightAnswers;
	int wrongAnswers;
	double avgTimeForQuestion;
};

inline void to_json(json& j, const GetMyStatusResponse& p)
{
	j = json{ { "numOfGames", p.numOfGames },{ "rightAnswers", p.rightAnswers },{ "wrongAnswers", p.wrongAnswers },{ "avgTimeForQuestion", p.avgTimeForQuestion } };
}

inline void from_json(const json& j, GetMyStatusResponse& p)
{
	p.numOfGames = j.at("numOfGames").get<int>();
	p.rightAnswers = j.at("rightAnswers").get<int>();
	p.wrongAnswers = j.at("wrongAnswers").get<int>();
	p.avgTimeForQuestion = j.at("avgTimeForQuestion").get<double>();
}