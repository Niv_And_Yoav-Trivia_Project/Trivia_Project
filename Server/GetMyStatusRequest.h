#pragma once

#include <iostream>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct GetMyStatusRequest
{
	string username;
};

inline void to_json(json& j, const GetMyStatusRequest& p)
{
	j = json{ { "username", p.username } };
}

inline void from_json(const json& j, GetMyStatusRequest& p)
{
	p.username = j.at("username").get<string>();
}