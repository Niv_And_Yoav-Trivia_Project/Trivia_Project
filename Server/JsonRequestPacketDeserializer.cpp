#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(vector<uint8_t> buffer)
{
	string str(buffer.begin() + 5, buffer.end());
	json j = json::parse(str);
	LoginRequest request = j;
	return request;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(vector<uint8_t> buffer)
{
	string str(buffer.begin() + 5, buffer.end());
	json j = json::parse(str);
	SignupRequest request = j;
	return request;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(vector<uint8_t> buffer)
{
	string str(buffer.begin() + 5, buffer.end());
	json j = json::parse(str);
	GetPlayersInRoomRequest request = j;
	return request;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(vector<uint8_t> buffer)
{
	string str(buffer.begin() + 5, buffer.end());
	json j = json::parse(str);
	JoinRoomRequest request = j;
	return request;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(vector<uint8_t> buffer)
{
	string str(buffer.begin() + 5, buffer.end());
	json j = json::parse(str);
	CreateRoomRequest request = j;
	return request;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(vector<uint8_t> buffer)
{
	string str(buffer.begin() + 5, buffer.end());
	json j = json::parse(str);
	SubmitAnswerRequest request = j;
	return request;
}

GetMyStatusRequest JsonRequestPacketDeserializer::deserializeGetMyStatusRequest(vector<uint8_t> buffer)
{
	string str(buffer.begin() + 5, buffer.end());
	json j = json::parse(str);
	GetMyStatusRequest request = j;
	return request;
}