#pragma once

#include <vector>
#include "SqliteDatabase.h"
#include "LoggedUser.h"

#define USERNAME_ALREADY_EXIST 2
#define SUCCEEDED 1
#define USERNAME_DOES_NOT_EXIST 0
#define UNABLE_TO_SIGNUP 0

class LoginManager
{
private:
	IDatabase& m_database;
	vector<LoggedUser> m_loggedUsers;
	
public:
	LoginManager(IDatabase& db);
	bool signup(string username, string password, string email);
	int login(string username, string password);
	bool logout(string username);

	IDatabase& GetDataBase() const;
	vector<LoggedUser> GetLoggedUsers() const;
	void SetDataBase(IDatabase& db);
	void SetLoggedUsers(vector<LoggedUser> loggedUsers);

	void operator=(const LoginManager& manager);
};
