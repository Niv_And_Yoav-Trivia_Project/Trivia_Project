#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "GameRequestHandler.h"

RequestHandlerFactory::RequestHandlerFactory(LoginManager& loginManager, RoomManager& roomManager, HighscoreTable& table, GameManager& gameManager) : m_loginManager(loginManager), m_roomManager(roomManager), m_highscoreTable(table), m_gameManager(gameManager)
{
}

LoginRequestHandler RequestHandlerFactory::createLoginRequestHandler()
{
	return LoginRequestHandler(m_loginManager, *this);
}

LoginManager& RequestHandlerFactory::GetLoginManager() const
{
	return m_loginManager;
}

void RequestHandlerFactory::operator=(const RequestHandlerFactory& factory)
{
	this->m_loginManager = factory.GetLoginManager();
}

MenuRequestHandler RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return MenuRequestHandler(user, m_roomManager, m_highscoreTable, *this);
}

RoomAdminRequestHandler RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, Room room)
{
	return RoomAdminRequestHandler(room, user, m_roomManager, *this);
}

RoomMemberRequestHandler RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, Room room)
{
	return RoomMemberRequestHandler(room, user, m_roomManager, *this);
}

GameRequestHandler RequestHandlerFactory::createGameRequestHandler(LoggedUser user, Room room)
{
	vector<Game*> games(m_gameManager.getGames());
	int position = 0;

	bool gameExist = false;
	for (int i = 0; i < games.size(); i++)
	{
		map<LoggedUser, GameData> myMap(games[i]->getPlayers());

		map<LoggedUser, GameData>::iterator it;

		for (it = myMap.begin(); it != myMap.end(); it++)
		{
			if (it->first.getUsername() == user.getUsername())
			{
				gameExist = true;
				position = i;
			}
		}
	}
	if (!gameExist)
	{
		Game* g(m_gameManager.createGame(room));
		return GameRequestHandler(*g, user, m_gameManager, *this);
	}
	else
	{
		Game* g((m_gameManager.getGames()[position]));
		return GameRequestHandler(*g, user, m_gameManager, *this);
	}
}