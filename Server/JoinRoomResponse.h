#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct JoinRoomResponse
{
	unsigned int status;
};

inline void to_json(json& j, const JoinRoomResponse& p)
{
	j = json{ { "status", p.status } };
}

inline void from_json(const json& j, JoinRoomResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}