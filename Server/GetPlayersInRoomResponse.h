#pragma once

#include <string>
#include <vector>
#include <iostream>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct GetPlayersInRoomResponse
{
	vector<string> players;
};

inline void to_json(json& j, const GetPlayersInRoomResponse& p)
{
	j = json{ { "players", p.players } };
}

inline void from_json(const json& j, GetPlayersInRoomResponse& p)
{
	p.players = j.at("players").get<vector<string>>();
}