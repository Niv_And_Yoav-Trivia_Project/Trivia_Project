#pragma once

#include <iostream>
#include <string>
#include <map>
#include "json.hpp"

using nlohmann::json;
using namespace std;

struct PlayerResults
{
	string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};

inline void to_json(json& j, const PlayerResults& p)
{
	j = json{ { "username", p.username },{ "correctAnswerCount", p.correctAnswerCount },{ "wrongAnswerCount", p.wrongAnswerCount },{ "averageAnswerTime", p.averageAnswerTime } };
}

inline void from_json(const json& j, PlayerResults& p)
{
	p.username = j.at("username").get<string>();
	p.correctAnswerCount = j.at("correctAnswerCount").get<unsigned int>();
	p.wrongAnswerCount = j.at("wrongAnswerCount").get<unsigned int>();
	p.averageAnswerTime = j.at("averageAnswerTime").get<unsigned int>();
}