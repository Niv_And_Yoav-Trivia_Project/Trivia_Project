#pragma comment (lib, "ws2_32.lib")
#include "Server.h"

int MenuRequestHandler::roomCount = 1;

int main()
{
	WSAInitializer wsaInit;

	IDatabase* db = new SqliteDatabase();
	db->addQuestion("How many ribs are there in a triangle?", "3", "2", "4", "5");
	db->addQuestion("How many ribs are there in a rectangle?", "4", "6", "3", "5");
	db->addQuestion("What temperature does water boil at?", "100C", "50C", "0C", "200C");
	db->addQuestion("This extension refers usually to what kind of file? (.PNG)", "Image file", "Password list", "Programming / scripting related file", "Audio file");
	db->addQuestion("What is the the nickname of Lebron James? ", "The King", "Black mamba", "The snake", "The tiger");
	db->addQuestion("Who won the NBA MVP in 2015? ", "Steph Curry", "Lebron James", "Russell Westbrook", "Kevin Wayne Durant");
	db->addQuestion("Which NBA team won the final in 2017? ", "Golden State Warrior", "Cleveland Cavaliers", "Boston", "Houston Rockets");
	db->addQuestion("Which team won the fifa World Cup in 2014? ", "Germany", "France", "Brazil", "Argentina");
	db->addQuestion("Where was the 2010 fifa World Cup ? ", "South Africa", "Germany", "Brazil", "France");
	db->addQuestion("Where was the 2014 fifa World Cup ? ", "Brazil", "Germany", "South Africa", "France");
	LoginManager* loginManager = new LoginManager(*db);
	RoomManager* roomManager = new RoomManager();
	GameManager* gameManager = new GameManager(*db);
	HighscoreTable table(*db);
	RequestHandlerFactory factory(*loginManager, *roomManager, table, *gameManager);
	Communicator communicator(factory);
	Server server(*db, communicator, factory);
	server.run();
	system("PAUSE");
	return 0;
}