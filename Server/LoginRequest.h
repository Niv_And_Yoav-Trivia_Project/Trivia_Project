#pragma once

#include <string>
#include <iostream>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct LoginRequest
{
	string username;
	string password;
};

inline void to_json(json& j, const LoginRequest& p)
{
	j = json{ { "username", p.username },{ "password", p.password } };
}

inline void from_json(const json& j, LoginRequest& p)
{
	p.username = j.at("username").get<string>();
	p.password = j.at("password").get<string>();
}