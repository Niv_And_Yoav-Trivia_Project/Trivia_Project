#pragma once

#include <string>
#include <iostream>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct SignupRequest
{
	string username;
	string password;
	string email;
};

inline void to_json(json& j, const SignupRequest& p)
{
	j = json{ { "username", p.username },{ "password", p.password },{ "email", p.email } };
}

inline void from_json(const json& j, SignupRequest& p)
{
	p.username = j.at("username").get<string>();
	p.password = j.at("password").get<string>();
	p.email = j.at("email").get<string>();
}