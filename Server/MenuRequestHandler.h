#pragma once

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include "RequestResult.h"
#include "RequestHandlerFactory.h"
#include "HighscoreTable.h"
#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

#define USERNAME_DOES_NOT_EXIST 0
#define ROOM_NAME_ALREADY_EXIST 0
#define ROOM_DOESNT_EXIST 0
#define UNABLE_TO_SIGNOUT 0
#define SUCCEEDED 1

class MenuRequestHandler : public IRequestHandler
{
private:
	LoggedUser m_user;
	RoomManager& m_roomManager;
	HighscoreTable& m_highscoreTable;
	RequestHandlerFactory& m_handlerFactory;
	static int roomCount;
	bool getRoomsFlag;

	RequestResult signout(Request);
	RequestResult getRooms(Request, SOCKET);
	RequestResult getPlayersInRoom(Request);
	RequestResult getHighscores(Request);
	RequestResult joinRoom(Request);
	RequestResult createRoom(Request);
	RequestResult myStatus(Request);

public:
	MenuRequestHandler(LoggedUser user, RoomManager& manager, HighscoreTable& table, RequestHandlerFactory& factory);
	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request, SOCKET);
	void logout();
	void getRoomsThread(Request request, SOCKET clientSocket);
};