#pragma once
#include "IDatabase.h"
#include <exception>

using namespace std;

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	void createTables();

	void addUser(string username, string password, string email);
	void addQuestion(string question, string correct_ans, string ans2, string ans3, string ans4);
	void addScore(int numOfGames, string username, int rightAnswers, int wrongAnswers, double avgTimeForQuestion);

	void removeUser(string username);
	void removeQuestion(string question);
	void removeScore(string username);

	list<Question> getQuestions();
	list<LoggedUser> getUsers();
	list<PlayerScore> getScores();

	map<LoggedUser, int> getHighscores();
	bool doesUsernameExist(string);
	bool doesUsernameAndPasswordExist(string, string);
	LoggedUser findUser(string, string);
	PlayerScore getScoreByUsername(string);
};