#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include "RequestResult.h"
#include "RequestHandlerFactory.h"
#include "HighscoreTable.h"
#include "LoginRequestHandler.h"
#include "GameRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

#define SUCCEEDED 1
#define UNABLE_TO_CLOSE_ROOM 0

class RoomAdminRequestHandler : public IRequestHandler
{
private:
	Room m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;
	bool getRoomStateFlag;
	bool roomClosed;
	bool gameStarted;

	RequestResult closeRoom(Request);
	RequestResult startGame(Request);
	RequestResult getRoomState(Request, SOCKET);

public:
	RoomAdminRequestHandler(Room room, LoggedUser user, RoomManager& manager, RequestHandlerFactory& factory);
	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request, SOCKET);
	void getRoomStateThread(Request request, SOCKET clientSocket);
	void logout();
};