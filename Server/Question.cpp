#include "Question.h"

Question::Question(string question, string correct_ans, string ans2, string ans3, string ans4)
{
	m_question = question;
	m_possibleAnswers.push_back(correct_ans);
	m_possibleAnswers.push_back(ans2);
	m_possibleAnswers.push_back(ans3);
	m_possibleAnswers.push_back(ans4);
}

string Question::getQuestion() const
{
	return m_question;
}

string Question::getCorrectAnswer() const
{
	return m_possibleAnswers[0];
}

vector<string> Question::getPossibleAnswers() const
{
	return m_possibleAnswers;
}

void Question::operator=(const Question& question)
{
	m_possibleAnswers = question.getPossibleAnswers();
	m_question = question.getQuestion();
}