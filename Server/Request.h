#pragma once

#include <ctime>
#include <vector>
#include <iostream>

using namespace std;

struct Request
{
	int id;
	time_t receivalTime;
	vector<uint8_t> buffer;
};
