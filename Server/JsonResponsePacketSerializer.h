#pragma once

#include <vector>
#include <iostream>
#include "ErrorResponse.h"
#include "LoginResponse.h"
#include "SignupResponse.h"
#include "LogoutResponse.h"
#include "GetPlayersInRoomResponse.h"
#include "GetRoomsResponse.h"
#include "JoinRoomResponse.h"
#include "CreateRoomResponse.h"
#include "HighscoreResponse.h"
#include "CloseRoomResponse.h"
#include "StartGameResponse.h"
#include "GetRoomStateResponse.h"
#include "LeaveRoomResponse.h"
#include "GetQuestionResponse.h"
#include "SubmitAnswerResponse.h"
#include "GetGameResultsResponse.h"
#include "GetMyStatusResponse.h"

using json = nlohmann::json;

using namespace std;

#define ERROR_CODE 0
#define LOGIN_CODE 1
#define SIGNUP_CODE 2

#define LOGOUT_CODE 3
#define GET_ROOMS_CODE 4
#define GET_PLAYERS_IN_ROOM_CODE 5
#define JOIN_ROOM_CODE 6
#define CREATE_ROOM_CODE 7
#define GET_HIGHSCORE_CODE 8

#define CLOSE_ROOM_CODE 9
#define START_GAME_CODE 10
#define GET_ROOM_STATE_CODE 11
#define LEAVE_ROOM_CODE 12

#define GET_QUESTION_CODE 13
#define SUBMIT_ANSWER_CODE 14
#define GET_GAME_RESULTS_CODE 15
#define LEAVE_GAME_CODE 16

#define GET_STATUS_CODE 17

class JsonResponsePacketSerializer
{
public:
	static vector<uint8_t> serializeResponse(ErrorResponse response);
	static vector<uint8_t> serializeResponse(LoginResponse response);
	static vector<uint8_t> serializeResponse(SignupResponse response);

	static vector<uint8_t> serializeResponse(LogoutResponse response);
	static vector<uint8_t> serializeResponse(GetRoomsResponse response);
	static vector<uint8_t> serializeResponse(GetPlayersInRoomResponse response);
	static vector<uint8_t> serializeResponse(JoinRoomResponse response);
	static vector<uint8_t> serializeResponse(CreateRoomResponse response);
	static vector<uint8_t> serializeResponse(HighscoreResponse response);

	static vector<uint8_t> serializeResponse(CloseRoomResponse response);
	static vector<uint8_t> serializeResponse(StartGameResponse response);
	static vector<uint8_t> serializeResponse(GetRoomStateResponse response);
	static vector<uint8_t> serializeResponse(LeaveRoomResponse response);

	static vector<uint8_t> serializeResponse(GetQuestionResponse response);
	static vector<uint8_t> serializeResponse(SubmitAnswerResponse response);
	static vector<uint8_t> serializeResponse(GetGameResultsResponse response);

	static vector<uint8_t> serializeResponse(GetMyStatusResponse response);
};	