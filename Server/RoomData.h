#pragma once
#include <string>
#include <iostream>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct RoomData
{
	unsigned int id;
	string name;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	unsigned int isActive;
	unsigned int questionCount;
};

inline void to_json(json& j, const RoomData& p)
{
	j = json{ { "id", p.id },{ "name", p.name },{ "maxPlayers", p.maxPlayers },{ "timePerQuestion", p.timePerQuestion },{ "isActive", p.isActive },{ "questionCount", p.questionCount } };
}

inline void from_json(const json& j, RoomData& p)
{
	p.id = j.at("id").get<unsigned int>();
	p.name = j.at("name").get<string>();
	p.maxPlayers = j.at("maxPlayers").get<unsigned int>();
	p.timePerQuestion = j.at("timePerQuestion").get<unsigned int>();
	p.isActive = j.at("isActive").get<unsigned int>();
	p.questionCount = j.at("questionCount").get<unsigned int>();
}