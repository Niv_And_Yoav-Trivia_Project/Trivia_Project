#include "User.h"


User::User(string username, string password, string email)
{
	_username = username;
	_password = password;
	_email = email;
}

string User::getUsername() const
{
	return _username;
}

string User::getPassword() const
{
	return _password;
}

string User::getEmail() const
{
	return _email;
}

void User::setUsername(string username)
{
	_username = username;
}

void User::setPassword(string password)
{
	_password = password;
}

void User::setEmail(string email)
{
	_email = email;
}

void User::operator=(const User& user)
{
	_username = user.getUsername();
	_password = user.getPassword();
	_email = user.getEmail();
}