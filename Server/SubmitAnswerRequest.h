#pragma once

#include <iostream>
#include <string>
#include "json.hpp"

using namespace std;
using nlohmann::json;

struct SubmitAnswerRequest
{
	bool answeredCorrect;
	double answerTime;
};

inline void to_json(json& j, const SubmitAnswerRequest& p)
{
	j = json{ { "answeredCorrect", p.answeredCorrect },{ "answerTime", p.answerTime } };
}

inline void from_json(const json& j, SubmitAnswerRequest& p)
{
	p.answeredCorrect = j.at("answeredCorrect").get<bool>();
	p.answerTime = j.at("answerTime").get<double>();
}