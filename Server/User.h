#pragma once
#include <iostream>
#include <string>

using namespace std;

class User
{
private:
	string _username;
	string _password;
	string _email;

public:
	User(string username, string password, string email);

	string getUsername() const;
	string getPassword() const;
	string getEmail() const;

	void setUsername(string username);
	void setPassword(string password);
	void setEmail(string email);

	bool operator==(const User& other)
	{
		if (_username != other.getUsername())
			return false;
		if (_password != other.getPassword())
			return false;
		if (_email != other.getEmail())
			return false;
		return true;
	}

	void operator=(const User& user);
};