#pragma once
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Question
{
private:
	string m_question;
	vector<string> m_possibleAnswers;

public:
	Question(string question, string correct_ans, string ans2, string ans3, string ans4);

	string getQuestion() const;
	string getCorrectAnswer() const;
	vector<string> getPossibleAnswers() const;

	bool operator==(const Question& other)
	{
		if (m_question != other.getQuestion())
			return false;
		if (this->getCorrectAnswer() != other.getCorrectAnswer())
			return false;
		if (this->getPossibleAnswers() != other.getPossibleAnswers())
			return false;
		return true;
	}

	void operator=(const Question& question);
};