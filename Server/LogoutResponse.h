#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct LogoutResponse
{
	unsigned int status;
};

inline void to_json(json& j, const LogoutResponse& p)
{
	j = json{ { "status", p.status } };
}

inline void from_json(const json& j, LogoutResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}