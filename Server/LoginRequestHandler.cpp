#include "LoginRequestHandler.h"

using json = nlohmann::json;

LoginRequestHandler::LoginRequestHandler(LoginManager& manager, RequestHandlerFactory& factory) : m_loginManager(manager), m_handlerFactory(factory)
{
}

RequestResult LoginRequestHandler::login(Request request)
{
	string username = JsonRequestPacketDeserializer::deserializeLoginRequest(request.buffer).username;
	string password = JsonRequestPacketDeserializer::deserializeLoginRequest(request.buffer).password;
	int status = m_loginManager.login(username, password);
	if (status == SUCCEEDED)
	{
		LoginResponse response { SUCCEEDED };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		IRequestHandler* handler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(username));
		RequestResult result{ buffer, handler };
		return result;
	}
	if (status == USERNAME_ALREADY_EXIST)
	{
		LoginResponse response { USERNAME_ALREADY_EXIST };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		IRequestHandler* handler = new MenuRequestHandler(m_handlerFactory.createMenuRequestHandler(username));
		RequestResult result{ buffer, this };
		return result;
	}
	else
	{
		LoginResponse response { USERNAME_DOES_NOT_EXIST };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
}

RequestResult LoginRequestHandler::signup(Request request)
{
	string username = JsonRequestPacketDeserializer::deserializeSignupRequest(request.buffer).username;
	string password = JsonRequestPacketDeserializer::deserializeSignupRequest(request.buffer).password;
	string email = JsonRequestPacketDeserializer::deserializeSignupRequest(request.buffer).email;
	if (m_loginManager.signup(username, password, email))
	{
		SignupResponse response { SUCCEEDED };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
	else
	{
		SignupResponse response { UNABLE_TO_SIGNUP };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result{ buffer, this };
		return result;
	}
}

bool LoginRequestHandler::isRequestRelevant(Request req)
{
	int code = (int)req.buffer[0];
	if (code >= LOGIN_CODE && code <= SIGNUP_CODE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

RequestResult LoginRequestHandler::handleRequest(Request request, SOCKET clientSocket)
{
	if (request.id == LOGIN_CODE)
	{
		return login(request);
	}
	else if (request.id == SIGNUP_CODE)
	{
		return signup(request);
	}
	else
	{
		ErrorResponse response{ "Code doesnt exist" };
		vector<uint8_t> buffer = JsonResponsePacketSerializer::serializeResponse(response);
		RequestResult result {buffer, this};
		return result;
	}
}