#include "LoggedUser.h"

LoggedUser::LoggedUser(string user)
{
	m_username = user;
}

LoggedUser::LoggedUser()
{
	m_username = "";
}

string LoggedUser::getUsername() const
{
	return m_username;
}

bool LoggedUser::operator==(const LoggedUser& other)
{
	return (this->getUsername() == other.getUsername());
}

void LoggedUser::setUsername(string username)
{
	m_username = username;
}
