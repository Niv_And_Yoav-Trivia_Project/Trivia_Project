#pragma once

#include "Communicator.h"

class Server
{
private:
	IDatabase& m_database;
	Communicator m_communicator;
	RequestHandlerFactory& m_handlerFactory;

public:
	Server(IDatabase& db, Communicator communicator, RequestHandlerFactory& factory);
	void run();
};
