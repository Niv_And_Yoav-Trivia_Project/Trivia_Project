#pragma once
#include "SqliteDatabase.h"
#include <map>
#include <iostream>
#include <utility> 
#include "LoggedUser.h"
#include <tuple>

class HighscoreTable
{
private:
	IDatabase & m_database;

public:
	HighscoreTable(IDatabase& db);
	vector<pair<int, string>> getHighscores();
};