#pragma once

#include <iostream>
#include "json.hpp"

using nlohmann::json;

struct LeaveRoomResponse
{
	unsigned int status;
};

inline void to_json(json& j, const LeaveRoomResponse& p)
{
	j = json{ { "status", p.status } };
}

inline void from_json(const json& j, LeaveRoomResponse& p)
{
	p.status = j.at("status").get<unsigned int>();
}