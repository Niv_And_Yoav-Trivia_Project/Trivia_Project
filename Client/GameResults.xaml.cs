﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for GameResults.xaml
    /// </summary>
    public partial class GameResults : Window
    {
        TcpClient _clientSocket;
        string _username;

        public GameResults(string[] names, string[] usersCorrectAnswers, string username, TcpClient temp)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            _username = username;
            _clientSocket = temp;

            int numOfUsers = names.Length;

            RowDefinition[] grids = new RowDefinition[numOfUsers];

            ColumnDefinition c1 = new ColumnDefinition();
            var converter = new GridLengthConverter();
            c1.Width = (GridLength)converter.ConvertFromString("*");

            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = (GridLength)converter.ConvertFromString("*");

            DynamicGrid.ColumnDefinitions.Add(c1);
            DynamicGrid.ColumnDefinitions.Add(c2);

            for (int i = 0; i < numOfUsers; i++)
            {
                grids[i] = new RowDefinition();
                grids[i].Height = (GridLength)converter.ConvertFromString("*");
                DynamicGrid.RowDefinitions.Add(grids[i]);

                Label user = new Label();
                user.Content = "user: " + names[i];
                user.FontSize = 22;
                user.FontWeight = FontWeights.Bold;
                user.Margin = new Thickness(85, 0, 0, 0);
                user.Foreground = System.Windows.Media.Brushes.White;
                user.HorizontalAlignment = HorizontalAlignment.Center;
                user.VerticalAlignment = VerticalAlignment.Center;
                Grid.SetRow(user, i);
                Grid.SetColumn(user, 0);
                DynamicGrid.Children.Add(user);

                Label score = new Label();
                score.Content = "score: " + usersCorrectAnswers[i];
                score.FontSize = 22;
                score.FontWeight = FontWeights.Bold;
                score.Margin = new Thickness(30, 0, 0, 0);
                score.Foreground = System.Windows.Media.Brushes.White;
                score.HorizontalAlignment = HorizontalAlignment.Left;
                score.VerticalAlignment = VerticalAlignment.Center;
                Grid.SetRow(score, i);
                Grid.SetColumn(score, 1);
                DynamicGrid.Children.Add(score);
            }

            RowDefinition gridRow2 = new RowDefinition();
            gridRow2.Height = new GridLength(80);
            DynamicGrid.RowDefinitions.Add(gridRow2);

            Button OKbutton = new Button();
            OKbutton.Height = 50;
            OKbutton.Width = 300;
            OKbutton.Content = "OK";
            OKbutton.FontSize = 30;
            OKbutton.Click += OKbuttonClick;
            OKbutton.FontWeight = FontWeights.Bold;
            DynamicGrid.Children.Add(OKbutton);

            Grid.SetRow(OKbutton, 6);
            OKbutton.SetValue(Grid.ColumnSpanProperty, 2);
        }

        private void OKbuttonClick(object sender, RoutedEventArgs e)
        {
            Menu m = new Menu(_username, _clientSocket);
            m.Show();
            this.Close();
        }
    }
}
