﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public const int SIGN_OUT_CODE = 3;

        TcpClient _clientSocket;
        string _username;

        public Menu(string username, TcpClient temp)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            HelloUser.Content = ("Hello " + username);
            _clientSocket = temp;
            _username = username;
        }
        private void SignOutButton(object sender, RoutedEventArgs e)
        {
            int code = SIGN_OUT_CODE;
            int length = 0;
            string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                // Read can return anything from 0 to numBytesToRead. 
                // This method blocks until at least one byte is read.
                netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);

                if (returndata.Contains("\"status\":1"))
                {
                    LogIn mainWin = new LogIn(_clientSocket);
                    mainWin.Show();
                    this.Close();
                }
            }
        }

        private void QuitButton(object sender, RoutedEventArgs e)
        {
            this.Close();
            _clientSocket.Close();
            System.Windows.Application.Current.Shutdown();
        }

        private void CreateRoomButton(object sender, RoutedEventArgs e)
        {
            CreateRoom c = new CreateRoom(_username, _clientSocket);
            c.Show();
            this.Close();
        }

        private void JoinRoomButton(object sender, RoutedEventArgs e)
        {
            JoinRoom j = new JoinRoom(_username, _clientSocket);
            j.Show();
            this.Close();
        }

        private void BestScoreButton(object sender, RoutedEventArgs e)
        {
            BestScore j = new BestScore(_username, _clientSocket);
            j.Show();
            this.Close();
        }

        private void MyStatusButton(object sender, RoutedEventArgs e)
        {
            MyStatus j = new MyStatus(_username, _clientSocket);
            j.Show();
            this.Close();
        }
    }
}
