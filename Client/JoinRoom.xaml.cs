﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Threading;
using System.IO;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : Window
    {
        public const int JOIN_ROOM_CODE = 6;
        public const int GET_ROOMS_CODE = 4;
        public const int GET_PLAYERS_IN_ROOM_CODE = 5;

        TcpClient _clientSocket;
        string _username;
        Dictionary<string, int> rooms = new Dictionary<string, int>();
        private BackgroundWorker background_worker = new BackgroundWorker();
        ThreadStart ts1;
        Thread t1;
        bool pressBack = false;
        bool pressJoin = false;

        public JoinRoom(string username, TcpClient temp)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            _clientSocket = temp;
            _username = username;

            UsersList.Visibility = Visibility.Collapsed;

            ts1 = new ThreadStart(background_worker_DoWork);
            t1 = new Thread(ts1);
            t1.IsBackground = true;
            t1.Start();
        }

        void background_worker_DoWork()
        {
            int code = GET_ROOMS_CODE;
            int length = 0;
            string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            while (pressJoin == false && pressBack == false)
            {
                if (netStream.CanRead)
                {
                    // Reads NetworkStream into a byte buffer.
                    byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                    // Read can return anything from 0 to numBytesToRead. 
                    // This method blocks until at least one byte is read.
                    netStream.ReadTimeout = 100;
                    try
                    {
                        netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);
                    }
                    catch (IOException ex)
                    {
                        continue;
                    }

                    // Returns the data received from the host to the console.
                    string returndata = Encoding.UTF8.GetString(bytes);

                    if (returndata.Split('[')[0].Contains("players"))
                    {
                        string[] names = returndata.Split('[')[1].Split(']')[0].Split(',');
                        List<string> items = new List<string>();
                        foreach (string name in names)
                        {
                            items.Add(name.Replace('"', ' '));
                        }
                        bool isEmpty = !items.Any();
                        if (isEmpty)
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                UsersList.Visibility = Visibility.Collapsed;
                            });
                        }
                        else
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                UsersList.Visibility = Visibility.Visible;
                                UsersList.Items.Clear();
                                int count = 0;
                                foreach (string item in items)
                                {
                                    if (count > 0)
                                    {
                                        UsersList.Items.Add(new Separator());
                                        UsersList.Items.Add(item);
                                    }
                                    else
                                    {
                                        UsersList.Items.Add(item);
                                    }
                                    count++;
                                }
                            });
                        }
                    }
                    else if (returndata.Split('[').Length == 1)
                    {
                        if (returndata.Contains("\"status\":1"))
                        {
                            pressJoin = true;
                            this.Dispatcher.Invoke(() =>
                            {
                                Room r = new Room(_username, _clientSocket, RoomList.SelectedItem.ToString(), false);
                                r.Show();
                                this.Close();
                            });
                            break;
                        }
                        if (returndata.Contains("\"status\":0"))
                        {
                            MessageBox.Show("Room full");
                            code = GET_ROOMS_CODE;
                            length = 0;
                            msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0;

                            netStream = _clientSocket.GetStream();

                            if (netStream.CanWrite)
                            {
                                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                                netStream.Write(sendBytes, 0, sendBytes.Length);
                            }
                        }
                    }
                    else
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            RoomList.Items.Clear();
                        });
                        rooms.Clear();
                        string data = returndata.Split('[')[1].Split(']')[0];
                        if (data != "")
                        {
                            int count = data.Split('{').Length - 1;
                            int counter = 0;
                            foreach (string line in data.Split('{'))
                            {
                                if (line != "")
                                {
                                    string roomData = line.Split('}')[0];
                                    string name = roomData.Split(':')[4].Split('"')[1].Split('"')[0];
                                    string idStr = roomData.Split(':')[1].Split(',')[0];
                                    int id = Convert.ToInt32(idStr);
                                    this.Dispatcher.Invoke(() =>
                                    {
                                        if (counter > 0)
                                        {
                                            RoomList.Items.Add(new Separator());
                                            RoomList.Items.Add(name);
                                        }
                                        else
                                        {
                                            RoomList.Items.Add(name);
                                        }
                                        counter++;
                                    });
                                    rooms.Add(name, id);
                                }
                            }
                        }
                    }
                }
                Thread.Sleep(500);
            }
            if (pressBack)
            {
                code = GET_ROOMS_CODE;
                length = 0;
                msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0;

                netStream = _clientSocket.GetStream();

                if (netStream.CanWrite)
                {
                    Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                    netStream.Write(sendBytes, 0, sendBytes.Length);
                }
            }
        }

        private void PlaceholdersListBox_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(sender as ListBox, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                int code = GET_PLAYERS_IN_ROOM_CODE;
                string likeJason = "{\"roomId\": " + rooms[item.Content.ToString()].ToString() + "}";
                int length = likeJason.Length;
                string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0 + likeJason;


                NetworkStream netStream = _clientSocket.GetStream();

                if (netStream.CanWrite)
                {
                    Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                    netStream.Write(sendBytes, 0, sendBytes.Length);
                }
            }
        }

        private void JoinClick(object sender, RoutedEventArgs e)
        {
            string item = RoomList.SelectedItem.ToString();
            if (item != null)
            {
                int code = GET_ROOMS_CODE;
                int length = 0;
                string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0;

                NetworkStream netStream = _clientSocket.GetStream();

                if (netStream.CanWrite)
                {
                    Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                    netStream.Write(sendBytes, 0, sendBytes.Length);
                }

                code = JOIN_ROOM_CODE;
                string likeJason = "{\"roomId\": " + rooms[item].ToString() + "}";
                length = likeJason.Length;
                msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0 + likeJason;
            
            
                netStream = _clientSocket.GetStream();
            
                if (netStream.CanWrite)
                {
                    Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                    netStream.Write(sendBytes, 0, sendBytes.Length);
                }
            }
        }

        private void BackButtun(object sender, RoutedEventArgs e)
        {
            pressBack = true;
            Menu u = new Menu(_username,_clientSocket);
            u.Show();
            this.Close();
        }
    }
}
