﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for MyStatus.xaml
    /// </summary>
    public partial class MyStatus : Window
    {
        TcpClient _clientSocket;
        string _username;
        public const int MY_STATUS_CODE = 17;

        public MyStatus(string username, TcpClient temp)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            _clientSocket = temp;
            _username = username;

            string likeJason = "{\"username\": \"" + _username + "\"}";
            int code = MY_STATUS_CODE;
            int length = likeJason.Length;
            string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0 + likeJason;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                // Read can return anything from 0 to numBytesToRead. 
                // This method blocks until at least one byte is read.
                netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);

                double time = Convert.ToDouble(returndata.Split(':')[1].Split(',')[0]);

                this.Dispatcher.Invoke(() =>
                {
                    avgTime.Text += String.Format("{0:0.00}", time); ;
                    numberOfGames.Text += returndata.Split(':')[2].Split(',')[0];
                    numberOfRightAnswers.Text += returndata.Split(':')[3].Split(',')[0];
                    numberOfWrongAnswers.Text += returndata.Split(':')[4].Split('}')[0];
                });
            }
        }

        private void BackButtonClick(object sender, RoutedEventArgs e)
        {
            Menu m = new Menu(_username, _clientSocket);
            m.Show();
            this.Close();
        }
    }
}