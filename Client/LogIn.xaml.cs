﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for LogIn.xaml
    /// </summary>
    public partial class LogIn : Window
    {
        public const int SIGN_IN_CODE = 1;

        TcpClient _clientSocket;

        public LogIn(TcpClient socket)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            _clientSocket = socket;
        }

        private void SignInButtun(object sender, RoutedEventArgs e)
        {
            string userName = UserName.Text;
            string password = Password.Password;

            if (userName == "" || password == "")
            {
                UnableWindow m = new UnableWindow("login", "Please fill all fields");
                m.ShowDialog();
                return;
            }

            string likeJason = "{\"username\": \"" + userName + "\", \"password\": \"" + password + "\"}";
            int code = SIGN_IN_CODE;
            int length = likeJason.Length;
            string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0 + likeJason;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                // Read can return anything from 0 to numBytesToRead. 
                // This method blocks until at least one byte is read.
                netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);

                if (returndata.Contains("\"status\":1"))
                {
                    Menu m = new Menu(userName, _clientSocket);
                    m.Show();
                    this.Close();
                }
                if (returndata.Contains("\"status\":0"))
                {
                    UnableWindow m = new UnableWindow("login", "Wrong username or password. Try again");
                    m.ShowDialog();
                }

                if (returndata.Contains("\"status\":2"))
                {
                    UnableWindow m = new UnableWindow("login", "User already logged in");
                    m.ShowDialog();
                }
            }
        }
        private void SignupButtun(object sender, RoutedEventArgs e)
        {
            SignUp subWindow = new SignUp(_clientSocket);
            subWindow.Show();
            this.Close();
        }
        private void ExitButtun(object sender, RoutedEventArgs e)
        {
            _clientSocket.Close();
            this.Close();
            System.Windows.Application.Current.Shutdown();
        }
    }
}
