﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.IO;

namespace ClientGUI
{
    public class GlobalVar
    {
        public static string PATH = "Test";
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int port;
        public string serverIp;
        public const int SIGN_IN_CODE = 1;

        public TcpClient _clientSocket = new TcpClient();
        public void connectToServer(string serverIp, int port)
        {
            try
            {
                _clientSocket.Connect(serverIp, port);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
                this.Close();
                System.Environment.Exit(1);
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            string configFile = @"config.txt";
            if (File.Exists(configFile))
            {
                string[] lines = System.IO.File.ReadAllLines(configFile);

                string[] words = lines[0].Split('=');
                if (words[0] == "server_ip")
                    serverIp = words[1];
                else if (words[0] == "port")
                    port = Int32.Parse(words[1]);
                else if (words[0] == "path")
                    GlobalVar.PATH = words[1];

                words = lines[1].Split('=');
                if (words[0] == "server_ip")
                    serverIp = words[1];
                else if (words[0] == "port")
                    port = Int32.Parse(words[1]);
                else if (words[0] == "path")
                    GlobalVar.PATH = words[1];

                words = lines[2].Split('=');
                if (words[0] == "server_ip")
                    serverIp = words[1];
                else if (words[0] == "port")
                    port = Int32.Parse(words[1]);
                else if (words[0] == "path")
                    GlobalVar.PATH = words[1];
            }
            else
            {
                MessageBox.Show("Error : couldn't find a config file");
                this.Close();
                System.Environment.Exit(1);
            }

            if (!_clientSocket.Connected)
            {
                connectToServer(serverIp, port);
            }

            LogIn mainWin = new LogIn(_clientSocket);
            mainWin.Show();
            this.Close();
        }
    }
   
}
