﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Window
    {
        public const int CREATE_ROOM_CODE = 7;

        TcpClient _clientSocket;
        string _username;

        public CreateRoom(string username, TcpClient temp)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            _clientSocket = temp;
            _username = username;
        }

        private void BackButton(object sender, RoutedEventArgs e)
        {
            Menu u = new Menu(_username, _clientSocket);
            u.Show();
            this.Close();
        }
        private void SendButton(object sender, RoutedEventArgs e)
        {
            string time = Time.Text;
            string name = Name.Text;
            string players = Players.Text;
            string questions = Questions.Text;

            if (time == "" || name == "" || players == "" || questions == "")
            {
                UnableWindow m = new UnableWindow("create room", "Please fill all fields");
                m.ShowDialog();
                return;
            }
            else if (!(int.TryParse(time, out int n)) || !(int.TryParse(players, out n)) || !(int.TryParse(questions, out n)))
            {
                UnableWindow m = new UnableWindow("create room", "Please enter valid input");
                m.ShowDialog();
                return;
            }
            else if (Int32.Parse(players) < 1)
            {
                UnableWindow m = new UnableWindow("create room", "wrong number of players");
                m.ShowDialog();
                return;
            }
            else if (Int32.Parse(questions) < 1)
            {
                UnableWindow m = new UnableWindow("create room", "wrong namber of question");
                m.ShowDialog();
                return;
            }
            else if (Int32.Parse(time) < 1)
            {
                UnableWindow m = new UnableWindow("create room", "wrong time");
                m.ShowDialog();
                return;
            }

            int code = CREATE_ROOM_CODE;
            string likeJason = "{\"roomName\": \"" + name + "\", \"maxUsers\": " + players + ", \"questionCount\": " + questions + ", \"answerTimeout\": " + time + "}";
            int length = likeJason.Length;
            string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0 + likeJason;


            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                // Read can return anything from 0 to numBytesToRead. 
                // This method blocks until at least one byte is read.
                netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);

                if (returndata.Contains("\"status\":1"))
                {
                    Room r = new Room(_username, _clientSocket, name, true);
                    r.Show();
                    this.Close();
                }
                else if (returndata.Contains("\"status\":0"))
                {
                    UnableWindow m = new UnableWindow("create room", "Room name is already exists, try again");
                    m.ShowDialog();
                }
            }
        }
    }
}
