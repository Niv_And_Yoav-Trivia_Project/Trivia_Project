﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for BestScore.xaml
    /// </summary>
    public partial class BestScore : Window
    {
        TcpClient _clientSocket;
        string _username;
        public const int GET_HIGHSCORE_CODE = 8;

        public BestScore(string username, TcpClient temp)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            _clientSocket = temp;
            _username = username;

            string user1Str = "";
            string user2Str = "";
            string user3Str = "";
            int code = GET_HIGHSCORE_CODE;
            string msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                // Read can return anything from 0 to numBytesToRead. 
                // This method blocks until at least one byte is read.
                netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);

                if (returndata != "")
                {
                    string[] names = returndata.Split(':')[1].Split(']');
                    if (names.Length > 2)
                    {
                        if (names[2].Contains(',') && !names[2].Contains("\"status\""))
                        {
                            user1Str = names[2].Split(',')[2].Replace('"', ' ') + "- " + names[2].Split(',')[1].Split('[')[1];
                        }
                        if (names[1].Contains(',') && !names[1].Contains("\"status\""))
                        {
                            user2Str = names[1].Split(',')[2].Replace('"', ' ') + "- " + names[1].Split(',')[1].Split('[')[1];
                        }
                        if (names[0].Contains(',') && !names[0].Contains("\"status\""))
                        {
                            user3Str = names[0].Split(',')[1].Replace('"', ' ') + "- " + names[0].Split(',')[0].Split('[')[2];
                        }
                    }

                    this.Dispatcher.Invoke(() =>
                    {
                        if (user1Str == "" && user2Str == "")
                            user1.Content = user3Str;
                        if (user1Str == "" && user2Str != "")
                        {
                            user1.Content = user2Str;
                            user2.Content = user3Str;
                        }
                        if (user1Str != "" && user2Str != "" && user3Str != "")
                        {
                            user1.Content = user1Str;
                            user2.Content = user2Str;
                            user3.Content = user3Str;
                        }
                    });
                }
            }
        }

        private void BackButtonClick(object sender, RoutedEventArgs e)
        {
            Menu m = new Menu(_username, _clientSocket);
            m.Show();
            this.Close();
        }

    }
}