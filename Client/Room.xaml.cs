﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.IO;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for Room.xaml
    /// </summary>
    public partial class Room : Window
    {
        public const int GET_ROOM_DATA_CODE = 11;
        public const int START_GAME_CODE = 10;
        public const int LEAVE_ROOM_CODE = 12;
        public const int CLOSE_ROOM_CODE = 9;

        TcpClient _clientSocket;
        string _username;
        string _roomName;
        bool _isManager;
        bool _hasGameBegun;
        bool _hasExitRoom;
        string _status;
        string _answerTimeout;
        string _questionCount;
        private BackgroundWorker background_worker = new BackgroundWorker();

        void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int code = GET_ROOM_DATA_CODE;
            string msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;
            string hasGameBegun;
            string players;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            while (_hasGameBegun == false)
            {
                if (_hasExitRoom == true)
                {
                    background_worker.CancelAsync();
                }

                if (background_worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    if (netStream.CanRead)
                    {
                        // Reads NetworkStream into a byte buffer.
                        byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                        // Read can return anything from 0 to numBytesToRead. 
                        // This method blocks until at least one byte is read.
                        netStream.ReadTimeout = 100;
                        try
                        {
                            netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);
                        }
                        catch (IOException ex)
                        {
                            continue;
                        }

                        // Returns the data received from the host to the console.
                        string returndata = Encoding.UTF8.GetString(bytes);
                        if (returndata != "")
                        {
                            if (returndata.Split(':')[0].Split('"')[1] == "answerTimeout")
                            {
                                _status = returndata.Split(':')[6].Split('}')[0];
                                _answerTimeout = returndata.Split(':')[1].Split(',')[0];
                                hasGameBegun = returndata.Split(':')[2].Split(',')[0];
                                players = returndata.Split(':')[4];
                                _questionCount = returndata.Split(':')[5].Split(',')[0];
                                string maxPlayers = returndata.Split(':')[3].Split(',')[0];


                                if (hasGameBegun == "false")
                                    _hasGameBegun = false;
                                if (hasGameBegun == "true")
                                    _hasGameBegun = true;
                                this.Dispatcher.Invoke(() =>
                                {
                                    RoomQuestion.Content = "Number of question: " + _questionCount;
                                    RoomTime.Content = "Time per question: " + _answerTimeout;
                                    RoomMaxPlayer.Content = "Max number players: " + maxPlayers;
                                });

                                string[] names = players.Split('[')[1].Split(']')[0].Split(',');
                                List<string> items = new List<string>();
                                foreach (string name in names)
                                {
                                    items.Add(name.Replace('"', ' '));
                                }
                                this.Dispatcher.Invoke(() =>
                                {
                                    UserList.ItemsSource = items;
                                });
                            }
                        }
                    }
                    if (_status == "0")
                        background_worker.CancelAsync();
                    if (_hasGameBegun == true)
                        background_worker.ReportProgress(100);
                    Thread.Sleep(1000);
                }
            }

            if (!_hasExitRoom && (_hasGameBegun && !_isManager))
            {
                code = GET_ROOM_DATA_CODE;
                msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;

                netStream = _clientSocket.GetStream();

                if (netStream.CanWrite)
                {
                    Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                    netStream.Write(sendBytes, 0, sendBytes.Length);
                }
            }
        }

        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (_isManager)
                {
                    Menu m = new Menu(_username, _clientSocket);
                    m.Show();
                    this.Close();
                }
                else if (!_hasExitRoom)
                {
                    Label adminClosed = new Label();
                    adminClosed.Content = "The admin closed the room press OK to continue";
                    adminClosed.Margin = new Thickness(0, 500, 0, 0);
                    adminClosed.FontSize = 20;
                    adminClosed.Foreground = System.Windows.Media.Brushes.Red;
                    adminClosed.HorizontalAlignment = HorizontalAlignment.Center;
                    grid.Children.Add(adminClosed);

                    Button OKbutton = new Button();
                    OKbutton.Height = 60;
                    OKbutton.Width = 180;
                    OKbutton.Content = "OK";
                    OKbutton.Margin = new Thickness(0, 540, 0, 0);
                    OKbutton.FontSize = 20;
                    OKbutton.FontWeight = FontWeights.Bold;
                    OKbutton.Click += RoomCancelledClick;
                    grid.Children.Add(OKbutton);

                    var child = grid.Children.OfType<Button>().Where(x => x.Name == "LeaveRoom").First();
                    grid.Children.Remove(child);
                }
                else
                {
                    Menu m = new Menu(_username, _clientSocket);
                    m.Show();
                    this.Close();
                }
            }
            else
            {
                Game m = new Game(_username, _clientSocket, _roomName, _answerTimeout, _questionCount);
                m.Show();
                this.Close();
            }
        }

        public Room(string username, TcpClient temp, string roomName, bool manager)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            _clientSocket = temp;
            _username = username;
            _roomName = roomName;
            _isManager = manager;
            _hasGameBegun = false;
            _hasExitRoom = false;

            Connected.Text += _roomName;
            UserName.Content += _username;

            if (_isManager)
            {
                LeaveRoom.Content = "Close Room";
                Button StartGame = new Button();
                StartGame.Height = 80;
                StartGame.Width = 200;
                StartGame.Content = "Start Game";
                StartGame.Margin = new Thickness(0, 570, 0, 0);
                StartGame.FontSize = 30;
                StartGame.FontWeight = FontWeights.Bold;
                StartGame.Click += StartGameClick;
                this.mainPanel.Children.Add(StartGame);
            }
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;
            background_worker.DoWork += background_worker_DoWork;
            background_worker.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync();
        }

        private void StartGameClick(object sender, RoutedEventArgs e)
        {
            int code = GET_ROOM_DATA_CODE;
            string msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            _hasGameBegun = true;

            code = START_GAME_CODE;
            msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;

            netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                netStream.ReadTimeout = 100;
                try
                {
                    netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);
                }
                catch (IOException ex)
                {
                }

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);
            }
        }

        private void RoomCancelledClick(object sender, RoutedEventArgs e)
        {
            List<string> items = new List<string>();
            this.Dispatcher.Invoke(() =>
            {
                UserList.ItemsSource = items;
            });
            Menu m = new Menu(_username, _clientSocket);
            m.Show();
            this.Close();
        }

        private void LeaveRoomButton(object sender, RoutedEventArgs e)
        {
            int code = GET_ROOM_DATA_CODE;
            string msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            code = 0;
            _hasExitRoom = true;
            if (_isManager)
            {
                code = CLOSE_ROOM_CODE;
            }
            else
            {
                code = LEAVE_ROOM_CODE;
            }


            msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;

            netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }
            List<string> items = new List<string>();
            this.Dispatcher.Invoke(() =>
            {
                UserList.ItemsSource = items;
            });
        }
    }
}