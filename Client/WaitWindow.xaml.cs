﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for WaitWindow.xaml
    /// </summary>
    public partial class WaitWindow : Window
    {
        private BackgroundWorker background_worker = new BackgroundWorker();
        TcpClient _clientSocket;
        string[] _names;
        string[] _usersCorrectAnswers;
        string _username;

        public WaitWindow(string username, TcpClient temp)
        {
            InitializeComponent();

            _clientSocket = temp;
            _username = username;

            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;
            background_worker.DoWork += background_worker_DoWork;
            background_worker.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync();
        }

        void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool allFinished = false;
            NetworkStream netStream = _clientSocket.GetStream();

            while (!allFinished)
            {

                if (netStream.CanRead)
                {
                    // Reads NetworkStream into a byte buffer.
                    byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                    // Read can return anything from 0 to numBytesToRead. 
                    // This method blocks until at least one byte is read.
                    netStream.ReadTimeout = 100;
                    try
                    {
                        netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);
                        allFinished = true;
                    }
                    catch (IOException ex)
                    {
                    }

                    if (allFinished)
                    {
                        // Returns the data received from the host to the console.
                        string returndata = Encoding.UTF8.GetString(bytes);

                        string players = returndata.Split('[')[1].Split(']')[0];
                        string[] listPlayers = players.Split('{');

                        string[] names = new string[listPlayers.Length - 1];
                        string[] usersCorrectAnswers = new string[listPlayers.Length - 1];
                        int count = 0;

                        foreach (string player in listPlayers)
                        {
                            if (player != "")
                            {
                                string name = player.Split(':')[3].Split('"')[1];
                                string correctAnswers = player.Split(':')[2].Split(',')[0];

                                names[count] = name;
                                usersCorrectAnswers[count] = correctAnswers;
                                count++;
                            }
                        }
                        _names = names;
                        _usersCorrectAnswers = usersCorrectAnswers;
                        background_worker.CancelAsync();
                    }
                }

                if (background_worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    string text = "";
                    this.Dispatcher.Invoke(() =>
                    {
                        text = Block.Text;
                    });
                    if (text == "Wait for other players to finish the game...")
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            Block.Text = "Wait for other players to finish the game.";
                        });
                    }
                    else if (text == "Wait for other players to finish the game..")
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            Block.Text = "Wait for other players to finish the game...";
                        });
                    }
                    else if (text == "Wait for other players to finish the game.")
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            Block.Text = "Wait for other players to finish the game..";
                        });
                    }

                    System.Threading.Thread.Sleep(1000);
                }
            }
        }

        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                GameResults results = new GameResults(_names, _usersCorrectAnswers, _username, _clientSocket);
                results.Show();
                this.Close();
            }
        }
    }
}
