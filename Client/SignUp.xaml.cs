﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for SignUp.xaml
    /// </summary>
    public partial class SignUp : Window
    {
        public const int SIGN_UP_CODE = 2;

        TcpClient _clientSocket;

        public SignUp(TcpClient socket)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            _clientSocket = socket;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;
        }
        private void SignupButtun(object sender, RoutedEventArgs e)
        {
            string userName = UserName.Text;
            string password = Password.Text;
            string email = Email.Text;

            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
            }
            catch
            {
                UnableWindow m = new UnableWindow("signup", "Invalid email");
                m.ShowDialog();
                return;
            }

            if (userName == "" || password == "" || email == "")
            {
                UnableWindow m = new UnableWindow("signup", "Please fill all fields");
                m.ShowDialog();
                return;
            }

            int code = SIGN_UP_CODE;
            string likeJason = "{\"username\": \"" + userName + "\", \"password\": \"" + password + "\", \"email\": \"" + email + "\"}";
            int length = likeJason.Length;
            string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0 + likeJason;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                // Read can return anything from 0 to numBytesToRead. 
                // This method blocks until at least one byte is read.
                netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);

                if (returndata.Contains("{\"status\":1}"))
                {
                    LogIn mainWin = new LogIn(_clientSocket);
                    mainWin.Show();
                    this.Close();
                }
                if (returndata.Contains("{\"status\":0}"))
                {
                    UnableWindow m = new UnableWindow("signup", "Username already exists, try again");
                    m.ShowDialog();
                }
            }
        }
        private void BackButtun(object sender, RoutedEventArgs e)
        {
            LogIn mainWin = new LogIn(_clientSocket);
            mainWin.Show();
            this.Close();
        }

    }
}
