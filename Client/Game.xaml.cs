﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.IO;
using System.Windows.Threading;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        public const int GET_QUESTION = 13;
        public const int SUBMIT_ANSWER = 14;//did
        public const int GET_GAME_RESULT = 15;
        public const int LEAVE_GAME = 16;//work

        TcpClient _clientSocket;
        string _username;
        string _roomName;
        string _answerTimeout;
        string _questionCount;
        int _currentQuestionCount;
        string _currectAnswer;

        private DispatcherTimer timer = new DispatcherTimer();
        private DispatcherTimer timerSleep = new DispatcherTimer();
        private int time;
        private double sleepTime;

        public Game(string username, TcpClient temp, string roomName, string answerTime, string questionCount)
        {
            _clientSocket = temp;
            _username = username;
            _roomName = roomName;
            _answerTimeout = answerTime;
            _questionCount = questionCount;
            time = Convert.ToInt32(_answerTimeout);
            _currentQuestionCount = 0;

            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(GlobalVar.PATH, UriKind.Absolute));
            this.Background = myBrush;

            RoomName.Content = _roomName;
            TimeCount.Content = time.ToString();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += timer_Tick;

            timerSleep.Interval = TimeSpan.FromSeconds(0.5);
            timerSleep.Tick += timer_Tick_sleep;

            restartTimer();
        }

        public void restartTimer()
        {
            this.Dispatcher.Invoke(() =>
            {
                Answer1.Background = new SolidColorBrush(Colors.Gainsboro);
                Answer2.Background = new SolidColorBrush(Colors.Gainsboro);
                Answer3.Background = new SolidColorBrush(Colors.Gainsboro);
                Answer4.Background = new SolidColorBrush(Colors.Gainsboro);
            });

            time = Convert.ToInt32(_answerTimeout);
            _currentQuestionCount++;

            if (_currentQuestionCount > Convert.ToInt32(_questionCount))
            {
                gameOver();
            }
            else
            {
                if (getQuestion())
                {
                    QuestionsCount.Content = "Question " + _currentQuestionCount.ToString() + "/" + _questionCount;
                    TimeCount.Content = time.ToString();
                    timer.Start();
                }
                else
                {
                    gameOver();
                }
            }
        }

        public void gameOver()
        {
            int code = GET_GAME_RESULT;
            string msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;
            timer.Stop();
            timerSleep.Stop();

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            WaitWindow wait = new WaitWindow(_username, _clientSocket);
            wait.ShowDialog();
            this.Close();
        }


        public bool getQuestion()
        {
            bool ans = true;
            int code = GET_QUESTION;
            string msg = "" + (char)code + (char)0 + (char)0 + (char)0 + (char)0;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                // Read can return anything from 0 to numBytesToRead. 
                // This method blocks until at least one byte is read.
                netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);

                if (returndata.Contains("\"status\":0"))
                {
                    ans = false;//GAME OVER
                }
                if (returndata.Contains("\"status\":1"))
                {
                    string question = returndata.Split(':')[2].Split(',')[0];
                    //MessageBox.Show(question.Replace('"', ' '));
                    string[] answers = returndata.Split(':')[1].Split('[')[1].Split(']')[0].Split(',');
                    for (int i = 0; i < answers.Length; i++)
                    {
                        answers[i] = answers[i].Replace("\"", "");
                    }
                    _currectAnswer = answers[0];

                    Random rnd = new Random();
                    string[] MyRandomArray = answers.OrderBy(x => rnd.Next()).ToArray();
                    this.Dispatcher.Invoke(() =>
                    {
                        TheQuestion.Content = question.Replace('"', ' ');
                        Answer1.Content = MyRandomArray[0].Replace('"', ' ');
                        Answer2.Content = MyRandomArray[1].Replace('"', ' ');
                        Answer3.Content = MyRandomArray[2].Replace('"', ' ');
                        Answer4.Content = MyRandomArray[3].Replace('"', ' ');
                    });
                }

            }
            return ans;
        }

        private void Answer1Click(object sender, RoutedEventArgs e)
        {
            string ans = Answer1.Content.ToString();
            timer.Stop();
            if (checkAnswer(ans))
            {
                Answer1.Background = new SolidColorBrush(Colors.Green);
                Answer1.Style = Resources["MyButton"] as Style;
                submitAnswer(true, Convert.ToInt32(_answerTimeout) - time);
                sleep();
            }
            else
            {
                Answer1.Background = new SolidColorBrush(Colors.Red);
                Answer1.Style = Resources["MyButton"] as Style;
                submitAnswer(false, Convert.ToInt32(_answerTimeout) - time);
                sleep();
            }
        }

        private void Answer2Click(object sender, RoutedEventArgs e)
        {
            string ans = Answer2.Content.ToString();
            timer.Stop();
            if (checkAnswer(ans))
            {
                Answer2.Background = new SolidColorBrush(Colors.Green);
                Answer2.Style = Resources["MyButton"] as Style;
                submitAnswer(true, Convert.ToInt32(_answerTimeout) - time);
                sleep();
            }
            else
            {
                Answer2.Background = new SolidColorBrush(Colors.Red);
                Answer2.Style = Resources["MyButton"] as Style;
                submitAnswer(false, Convert.ToInt32(_answerTimeout) - time);
                sleep();
            }
        }

        private void Answer3Click(object sender, RoutedEventArgs e)
        {
            string ans = Answer3.Content.ToString();
            timer.Stop();
            if (checkAnswer(ans))
            {
                Answer3.Background = new SolidColorBrush(Colors.Green);
                Answer3.Style = Resources["MyButton"] as Style;
                submitAnswer(true, Convert.ToInt32(_answerTimeout) - time);
                sleep();
            }
            else
            {
                Answer3.Background = new SolidColorBrush(Colors.Red);
                Answer3.Style = Resources["MyButton"] as Style;
                submitAnswer(false, Convert.ToInt32(_answerTimeout) - time);
                sleep();
            }
        }

        private void Answer4Click(object sender, RoutedEventArgs e)
        {
            string ans = Answer4.Content.ToString();
            timer.Stop();
            if (checkAnswer(ans))
            {
                Answer4.Background = new SolidColorBrush(Colors.Green);
                Answer4.Style = Resources["MyButton"] as Style;
                submitAnswer(true, Convert.ToInt32(_answerTimeout) - time);
                sleep();
            }
            else
            {
                Answer4.Background = new SolidColorBrush(Colors.Red);
                Answer4.Style = Resources["MyButton"] as Style;
                submitAnswer(false, Convert.ToInt32(_answerTimeout) - time);
                sleep();
            }
        }

        public void submitAnswer(bool answer, int answerTime)
        {
            int code = SUBMIT_ANSWER;
            string boolLikeC = "";
            if (answer == true)
                boolLikeC = "true";
            if (answer == false)
                boolLikeC = "false";

            string likeJason = "{\"answeredCorrect\": " + boolLikeC + ", \"answerTime\": " + answerTime.ToString() + "}";
            int length = likeJason.Length;
            string msg = "" + (char)code + (char)length + (char)0 + (char)0 + (char)0 + likeJason;

            NetworkStream netStream = _clientSocket.GetStream();

            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes(msg);
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }

            if (netStream.CanRead)
            {
                // Reads NetworkStream into a byte buffer.
                byte[] bytes = new byte[_clientSocket.ReceiveBufferSize];

                // Read can return anything from 0 to numBytesToRead. 
                // This method blocks until at least one byte is read.
                netStream.Read(bytes, 0, (int)_clientSocket.ReceiveBufferSize);

                // Returns the data received from the host to the console.
                string returndata = Encoding.UTF8.GetString(bytes);
            }
        }

        public void sleep()
        {
            sleepTime = 0.5;
            timerSleep.Start();
        }

        public bool checkAnswer(string ans)
        {
            bool result = false;
            if (ans == _currectAnswer)
                result = true;
            return result;
        }

        public void wrongAnswer()
        {
            this.Dispatcher.Invoke(() =>
            {
                Answer1.Background = new SolidColorBrush(Colors.Red);
                Answer1.Style = Resources["MyButton"] as Style;
                Answer2.Background = new SolidColorBrush(Colors.Red);
                Answer2.Style = Resources["MyButton"] as Style;
                Answer3.Background = new SolidColorBrush(Colors.Red);
                Answer3.Style = Resources["MyButton"] as Style;
                Answer4.Background = new SolidColorBrush(Colors.Red);
                Answer4.Style = Resources["MyButton"] as Style;
            });
            sleep();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (time > 0)
            {
                time--;
                TimeCount.Content = string.Format("{0}", time % 60);
            }
            else
            {
                timer.Stop();
                submitAnswer(false, Convert.ToInt32(_answerTimeout));
                wrongAnswer();
            }

        }

        private void timer_Tick_sleep(object sender, EventArgs e)
        {
            if (sleepTime > 0)
            {
                sleepTime--;
            }
            else
            {
                timerSleep.Stop();
                Answer1.ClearValue(Button.StyleProperty);
                Answer2.ClearValue(Button.StyleProperty);
                Answer3.ClearValue(Button.StyleProperty);
                Answer4.ClearValue(Button.StyleProperty);
                restartTimer();
            }
        }
    }
}