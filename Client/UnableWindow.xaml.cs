﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for UnableWindow.xaml
    /// </summary>
    public partial class UnableWindow : Window
    {
        public UnableWindow(string topic, string msg)
        {
            InitializeComponent();

            TextBlock text = new TextBlock();
            text.Text = msg;
            Topic.Content = "Unable to " + topic;
            text.HorizontalAlignment = HorizontalAlignment.Center;
            text.VerticalAlignment = VerticalAlignment.Center;
            DynamicGrid.Children.Add(text);

            Grid.SetRow(text, 1);
        }

        private void OK_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
